
$(function() {
  $('#example1').DataTable({
    'pageLength':10,
  });
   $('[data-toggle="tooltip"]').tooltip();
})

$(document).on('keyup', '.ajaxSearch', function(e){
	"use strict";
    var input_data = $('#member_id').val();
    if (input_data.length === 0) {
        $('#suggestions').hide();
    } else {
        $.ajax({
            type: "POST",
            url: THEMEBASEURL+'circulation/search',
            data: {
              	'post_data'  : input_data,
              	[CSRFNAME]  : CSRFHASH,
            },
            success: function(data) {
                console.log(data);
                if (data != 'false') {
                    if (data.length > 0) {
                        $('#suggestions').show();
                        $('#autoSuggestionsList').addClass('auto_list');
                        $('#autoSuggestionsList').html(data);
                    }else{
                        $('#suggestions').show();
                        $('#autoSuggestionsList').addClass('auto_list');
                        $('#autoSuggestionsList').html('<a href="#/" class="memberID" id=""><li>No Records Found !</li></a>');
                    }
                } else {
                    $('#suggestions').show();
                    $('#autoSuggestionsList').addClass('auto_list');
                    $('#autoSuggestionsList').html('<a href="#/" class="memberID" id=""><li>No Records Found !</li></a>');
                }
            }
        });
    }
});


$(document).on('click', '.renewal', function(e){
    "use strict";
    var id = $(this).attr('data-id');
    var expiry_date = $(this).attr('data-date');
    var membershipID = $(this).attr('data-membershipID');
    swal({
    title: 'Are you sure?',
    text: "You Have Renewal this book!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, it is!',
    showLoaderOnConfirm: true,
        preConfirm: function(){
            return new Promise(function(resolve) {
                dataType: "json",
                $.ajax({
                    url: THEMEBASEURL+'circulation/renewal',
                    type: 'POST',
                    data: {
                        'id'                : id,
                        'expiry_date'       : expiry_date,
                        'membershipID'      : membershipID,
                        [CSRFNAME]          : CSRFHASH,
                    },
                    dataType: 'html'
                })
               .done(function(response){
                // swal.close();
                var response = jQuery.parseJSON(response);
                    if(response.confirmation == 'Success') {
                        swal({
                            title: "Successfully Renewal.",
                            position: 'top-end',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 1600,
                        });
                        setTimeout(function(){ window.location.reload();}, 1800);
                    }else if(response.confirmation == 'error'){
                        swal({
                            title: "Opps!",
                            text: "This Book Renewal Limit already Exist.",
                            position: 'top-end',
                            type: 'error',
                            showConfirmButton: true,

                        });
                    }else{
                        console.log(response);
                        swal.close();
                        $("#renewal_fee").modal();
                        $('#penalty_by').val('renewal');
                        $('#member_id_up').val(response.memberID);
                        $('#circulation_id_up').val(response.circulationID);
                        $('#circulation_code_up').val(response.circulationCode);
                        $('#book_id_up').val(response.bookID);
                        $('#member_penalty_fee').val(response.penalty_fee);
                        $('#member_book_limit_days').val(response.book_limit_days);
                        $('#no_of_days').val(response.date_count);
                        $('#member_penalty_amount').val(response.calculate_fee);
                    }

                })
                .fail(function(){
                    swal('Oops...', 'Something went wrong with You !', 'error');
                });
            });
        },
        allowOutsideClick: false        
    });

});


$(document).on('click', '.penalty', function(e){
  "use strict";
  var penalty_by            = $('#penalty_by').val();
  var circulation_id        = $('#circulation_id_up').val();
  var circulation_code      = $('#circulation_code_up').val();
  var member_id             = $('#member_id_up').val();
  var book_id               = $('#book_id_up').val();
  var penalty_fee           = $('#member_penalty_fee').val();
  var no_of_days            = $('#no_of_days').val();
  var penalty_amount        = $('#member_penalty_amount').val();
  var book_limit_days       = $('#member_book_limit_days').val();
  var member_penalty_note   = $('#member_penalty_note').val();

  if(circulation_id != 'NULL' || circulation_id != '') {
    dataType: "json",
    $.ajax({
      type: 'POST',
      url: THEMEBASEURL+'circulation/penalty',
      data: {
        'penalty_by'            : penalty_by,
        'circulation_id'        : circulation_id,
        'circulation_code'      : circulation_code,
        'member_id'             : member_id,
        'book_id'               : book_id,
        'penalty_fee'           : penalty_fee,
        'no_of_days'            : no_of_days,
        'penalty_amount'        : penalty_amount,
        'book_limit_days'       : book_limit_days,
        'member_penalty_note'   : member_penalty_note,
        [CSRFNAME]              : CSRFHASH,
      },
      dataType: "html",
      success: function(data) {
        var response = jQuery.parseJSON(data);
        console.log(response);
        if(response.confirmation == 'Success'){
        $('#renewal_fee').modal('toggle');
          swal({
            title: "Successfully",
            text: "The Penalty Has Been Successfully Paid And The Book Has Been Renewed.",
            position: 'top-end',
            type: 'success',
            showConfirmButton: false,
            timer: 1800,
          });
          setTimeout(function(){ window.location.reload();}, 3000);
        }
      }
    });
  }
});


$(document).on('click', '.return', function(e){
  "use strict";
    var id = $(this).attr('data-id');
    var expiry_date = $(this).attr('data-date');
    var membershipID = $(this).attr('data-membershipID');
    swal({
    title: 'Are you sure?',
    text: "You Have Return this book!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, it is!',
    showLoaderOnConfirm: true,

        preConfirm: function(){
            return new Promise(function(resolve) {
                dataType: "json",
                $.ajax({
                    url: THEMEBASEURL+'circulation/return',
                    type: 'POST',
                    data: {
                        'id'                : id,
                        'expiry_date'       : expiry_date,
                        'membershipID'      : membershipID,
                        [CSRFNAME]          : CSRFHASH,
                    },
                    dataType: 'html'
                })
               .done(function(response){
                // swal.close();
                var response = jQuery.parseJSON(response);
                    if(response.confirmation == 'Success') {
                        swal({
                            title: "Successfully Returned.",
                            position: 'top-end',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 1600,
                        });
                        setTimeout(function(){ window.location.reload();}, 1800);
                    }else{
                        console.log(response);
                        swal.close();
                        $("#renewal_fee").modal();
                        $('#penalty_by').val('returned');
                        $('#member_id_up').val(response.memberID);
                        $('#circulation_id_up').val(response.circulationID);
                        $('#circulation_code_up').val(response.circulationCode);
                        $('#book_id_up').val(response.bookID);
                        $('#member_penalty_fee').val(response.penalty_fee);
                        $('#member_book_limit_days').val(response.book_limit_days);
                        $('#no_of_days').val(response.date_count);
                        $('#member_penalty_amount').val(response.calculate_fee);
                    }

                })
                .fail(function(){
                    swal('Oops...', 'Something went wrong with You !', 'error');
                });
            });
        },
        allowOutsideClick: false        
    });
});



// lost script
$(document).on('click', '.lost', function(e){
    "use strict";
    var id = $(this).attr("id");
    swal({
    title: 'Are you sure?',
    text: "You Have Lost this book!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, it is!',
    showLoaderOnConfirm: true,

        preConfirm: function(){
            return new Promise(function(resolve) {
                $.ajax({
                    url: THEMEBASEURL+'circulation/lost',
                    type: 'POST',
                    data: {
                      'id'       : id,
                      [CSRFNAME]    : CSRFHASH,
                    },
                    dataType: 'json'
                })
               .done(function(response){
                swal.close();
                    $("#lost_book").modal();
                    $('#lost_circulationID').val(response.circulationID);
                    $('#lost_memberID').val(response.memberID);
                    $('#lost_bookID').val(response.bookID);
                    $('#book_price').val(response.book_price);
                    $('#payable_amount').val(response.book_price);
                })
                .fail(function(){
                    swal('Oops...', 'Something went wrong with You !', 'error');
                });
            });
        },
        allowOutsideClick: false        
    }); 
});




$(document).on('click', '.lost_penalty', function(e){
  "use strict";
  var circulationID      = $('#lost_circulationID').val();
  var memberID           = $('#lost_memberID').val();
  var bookID             = $('#lost_bookID').val();
  var book_price         = $('#book_price').val();
  var payable_amount     = $('#payable_amount').val();
  var lost_note          = $('#book_lost_note').val();


  if(circulationID != 'NULL' || circulationID != '') {
    dataType: "json",
    $.ajax({
      type: 'POST',
      url: THEMEBASEURL+'circulation/lost',
      data: {
        'circulationID'     : circulationID,
        'memberID'          : memberID,
        'bookID'            : bookID,
        'book_price'        : book_price,
        'payable_amount'    : payable_amount,
        'lost_note'         : lost_note,
        [CSRFNAME]          : CSRFHASH,
      },
      dataType: "html",
      success: function(data) {
        var response = jQuery.parseJSON(data);
        console.log(response);
        if(response.confirmation == 'Success'){
        $('#lost_book').modal('toggle');
          swal({
            title: "Successfully",
            text: "The Penalty Has Been Successfully Paid.",
            position: 'top-end',
            type: 'success',
            showConfirmButton: false,
            timer: 1800,
          });
          setTimeout(function(){ window.location.reload();}, 3000);
        }
      }
    });
  }
});