$(document).on('click', '.details', function(e){
  "use strict";
  var id = $(this).attr("id");
  if(id != 'NULL' || id != '') {
    $.ajax({
      	type: 'POST',
      	dataType: "json",
      	url: THEMEBASEURL+'payment/view',
	    data: {
	        'id'  : id,
	        [CSRFNAME]  : CSRFHASH,
	     },
	    dataType: "html",
	    success: function(data) {
	        $('#showData').html(data);
	    } 
    });
  }
});


// for print 
$(document).on('click', '.prints', function(e){
  $('#details').modal('toggle');
  var divElements = document.getElementById('print_areas').innerHTML;
  var oldPage = document.body.innerHTML;
  document.body.innerHTML = 
  "<html>"+
  "<head>"+
  "<title>Payment Slip</title><style type='text/css'>.modal-body{padding: 0px 15px 0px 15px !important; border-bottom: 2px dotted;}.receipt-header h6{line-height:7px !important;}.receipt-header h3{line-height:1px !important;}.border-dotted{border-bottom: 2px dotted}.border-top{border-top: 2px solid #000000;padding-left: 25px;padding-right: 25px;}.zero-padding{padding-left:0px; padding-right: 0px;}</style>"+
  "</head>"+
  "<body>" + 
  divElements + "</body>";
  window.print();
  document.body.innerHTML = oldPage;
  location.reload();
});