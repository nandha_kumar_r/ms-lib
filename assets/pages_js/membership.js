$(function(){
	$('#example1').DataTable({'pageLength':10,});
});

$(document).on('keypress', '.only_number', function(e){
    var yourInput = $(this).val();
    var keyCode = e.which ? e.which : e.keyCode
    if (!(keyCode >= 48 && keyCode <= 57)) {
        return false;
    }
});


$(document).on('keyup', '.is_numeric', function(e){
    var yourInput = $(this).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",A-z<>\{\} \[\]\\\/]/gi;
    var isCheck = re.test(yourInput);
    if(isCheck){
        var data = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",A-z<>\{\} \[\]\\\/]/gi, '');
        $(this).val(data);
    }
});

$(document).on('click', '.insert', function(e){
  	"use strict";
  	var membership_code     = $("#membership_code").val();
    var membership_name     = $("#membership_name").val();
  	var limit_book          = $("#membership_limit_book").val();
    var limit_day           = $("#membership_limit_day").val();
    var fee                 = $("#membership_fee").val();
    var penalty_fee         = $("#membership_penalty_fee").val();
    var renew_limit         = $("#membership_renew_limit").val();

  	$('#error_name').html('');
  	$('.error-name').removeClass('has-error');
  	$('#error_limit_book').html('');
  	$('.error-limit-book').removeClass('has-error');
    $('#error_limit_day').html('');
    $('.error-limit-day').removeClass('has-error');
    $('#error_renew_limit').html('');
    $('.error-renew-limit').removeClass('has-error');
  	$.ajax({
    	dataType: 'json',
    	type: 'POST',
    	url: THEMEBASEURL+'membership/add',
    	data: {
      		'membership_code'   : membership_code,
          'membership_name'   : membership_name,
      		'limit_book'        : limit_book, 
          'limit_day'         : limit_day, 
          'fee'               : fee, 
          'penalty_fee'       : penalty_fee, 
          'renew_limit'       : renew_limit, 
      		[CSRFNAME]          : CSRFHASH,
    	},
    	dataType: "html",
    	success: function(data){
      		var response = jQuery.parseJSON(data);
      		console.log(data);
      		if(response.confirmation == 'Success'){
    			$('#insert').modal('hide');
        		swal({
          			title: "Successfull.",
          			position: 'top-end',
          			type: 'success',
          			showConfirmButton: false,
          			timer: 1600,
        		});
        		setTimeout(function(){ window.location.href = THEMEBASEURL+'membership'; }, 1800);
      		}else{
        		$('#error_name').html(response.validations['membership_name']);
        		if (response.validations['membership_name']) {
          			$('.error-name').addClass('has-error');
        		};
        		$('#error_limit_book').html(response.validations['limit_book']);
        		if (response.validations['limit_book']){
          			$('.error-limit-book').addClass('has-error');
        		};
            $('#error_limit_day').html(response.validations['limit_day']);
            if (response.validations['limit_day']){
                $('.error-limit-day').addClass('has-error');
            };
            $('#error_renew_limit').html(response.validations['renew_limit']);
            if (response.validations['renew_limit']){
                $('.error-renew-limit').addClass('has-error');
            };
      		}
    	}
  	});
});

$(document).on('click', '.update', function(e){
  	"use strict";
	   var id = $(this).attr('id');
  	if(id != 'NULL' || id != '') {
		dataType: "json",
    	$.ajax({
      		type: 'POST',
      		url: THEMEBASEURL+'membership/retrive',
      		data: {
        		'id'  : id,
        		[CSRFNAME]  : CSRFHASH,
      		},
      		dataType: "html",
      		success: function(data) {
        		var response = jQuery.parseJSON(data);
        		console.log(response);
        		if(response.confirmation == 'Success') {
          			$('#membershipID').val(response.id);
          			$('#membership_name_up').val(response.membership_name);
          			$('#membership_books_limit_up').val(response.membership_books_limit);
                $('#membership_days_limit_up').val(response.membership_days_limit);
                $('#membership_fee_up').val(response.membership_fee);
                $('#membership_penalty_fee_up').val(response.penalty_fee);
                $('#membership_renew_limit_up').val(response.renew_limit);


          			$('.updated').click(function(){
			            var id                  = $('#membershipID').val();
			            var membership_name     = $("#membership_name_up").val();
                  var limit_book          = $("#membership_books_limit_up").val();
                  var limit_day           = $("#membership_days_limit_up").val();
                  var fee                 = $("#membership_fee_up").val();
                  var penalty_fee         = $("#membership_penalty_fee_up").val();
                  var renew_limit         = $("#membership_renew_limit_up").val();

			            $('#error_name_up').html('');
                  $('.error-name-up').removeClass('has-error');
                  $('#error_limit_book_up').html('');
                  $('.error-limit-book-up').removeClass('has-error');
                  $('#error_limit_day_up').html('');
                  $('.error-limit-day-up').removeClass('has-error');
                  $('#error_renew_limit-up').html('');
                  $('.error-renew-limit-up').removeClass('has-error');
            			
            			$.ajax({
              				dataType: 'json',
              				type: 'POST',
              				url: THEMEBASEURL+'membership/edit',
              				data: {
                				'id'                : id,
                				'membership_name'   : membership_name,
                        'limit_book'        : limit_book, 
                        'limit_day'         : limit_day, 
                        'fee'               : fee, 
                        'penalty_fee'       : penalty_fee, 
                        'renew_limit'       : renew_limit, 
                				[CSRFNAME]          : CSRFHASH,
              				},
              				dataType: "html",
              				success: function(data){
                				var response = jQuery.parseJSON(data);
                				console.log(data);
                				if(response.confirmation == 'Success'){
                  					$('#update').modal('hide');
                  					swal({
					                    title: "Successfully Updated.",
					                    position: 'top-end',
					                    type: 'success',
					                    showConfirmButton: false,
					                    timer: 1600,
                  					});
                  					setTimeout(function(){ window.location.href = THEMEBASEURL+'membership'; }, 1800);
                				} else {
                  				$('#error_name_up').html(response.validations['membership_name']);
                          if (response.validations['membership_name']) {
                              $('.error-name-up').addClass('has-error');
                          };
                          $('#error_limit_book_up').html(response.validations['limit_book']);
                          if (response.validations['limit_book']){
                              $('.error-limit-book-up').addClass('has-error');
                          };
                          $('#error_limit_day_up').html(response.validations['limit_day']);
                          if (response.validations['limit_day']){
                              $('.error-limit-day-up').addClass('has-error');
                          };
                          $('#error_renew_limit_up').html(response.validations['renew_limit']);
                          if (response.validations['renew_limit']){
                              $('.error-renew-limit-up').addClass('has-error');
                          };
	                			}
	              			}
	            		});
					});
				}
			}
		});
	}
});

$(document).on('click', '.delete', function(e){
  	"use strict";
  	var id = $(this).attr("id");
  	swal({
    	title: 'Are you sure?',
    	text: "It will be deleted permanently!",
    	type: 'warning',
    	showCancelButton: true,
    	confirmButtonColor: '#3085d6',
    	cancelButtonColor: '#d33',
    	confirmButtonText: 'Yes, delete it!',
    	showLoaderOnConfirm: true,

    	preConfirm: function() {
      		return new Promise(function(resolve) {
       			$.ajax({
        			url: THEMEBASEURL+'membership/delete',
        			type: 'POST',
        			data: {
          				'id'       : id,
          				[CSRFNAME]    : CSRFHASH,
    				},
        			dataType: 'json'
      			})
       			.done(function(response){
        			swal('Deleted!', response.message, response.status);
        			setTimeout(function(){ window.location.href = THEMEBASEURL+'membership'; }, 2000);
      			})
       			.fail(function(){
        			swal('Oops...', 'Something went wrong with You !', 'error');
      			});
     		});
    	},
    	allowOutsideClick: false        
  	}); 
});

// status script
$(document).on('click', '.onoffswitch-small-checkbox', function(e){
  	"use strict";
  	var status = '';
  	var id = 0;
  	if($(this).prop('checked')){
    	status = '1';
		id = $(this).parent().attr("id");
  	} else {
    	status = '0';
    	id = $(this).parent().attr("id");
  	}
  	if((status != '' || status != null) && (id !='')) {
    	$.ajax({
      		type: 'POST',
      		url: THEMEBASEURL+'membership/status',
      		data: {
        		'id'      : id,
        		'status'    : status,
        		[CSRFNAME]   : CSRFHASH,
      		},
      		dataType: "html",
      		success: function(data){
        		var response = jQuery.parseJSON(data);
        		console.log(data);
        		if(response.confirmation == 'Success'){
          			swal({
            			title: "Successfully Updated.",
            			position: 'top-end',
            			type: 'success',
            			showConfirmButton: false,
            			timer: 1600,
          			});
        		} 
      		}
    	});
  	}
});