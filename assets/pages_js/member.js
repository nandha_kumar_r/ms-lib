$(function () {
  $('#example1').DataTable({
    'pageLength':25,
  });
});


$(document).ready(function(){
    $('.select2').select2();
    $('[data-toggle="tooltip"]').tooltip();
});


$(document).on('keyup', '.is_numeric', function(e){
    var yourInput = $(this).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",A-z<>\{\} \[\]\\\/]/gi;
    var isCheck = re.test(yourInput);
    if(isCheck){
        var data = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",A-z<>\{\} \[\]\\\/]/gi, '');
        $(this).val(data);
    }
});

$(document).on('keypress', '.only_number', function(e){
    var yourInput = $(this).val();
    var keyCode = e.which ? e.which : e.keyCode
    if (!(keyCode >= 48 && keyCode <= 57)) {
        return false;
    }
});


$(document).on('click', '.changeUserPassword', function(e){
  "use strict";
  var id = $(this).attr('id');

  $('.changepassword').click(function(){
    var new_password = $('#new_password').val();
    var confirm_password = $('#confirm_password').val();
    $('#error_new_password').html('');
    $('#error_confirm_password').html('');
    $.ajax({
      dataType: "json",
      type: 'POST',
      url: THEMEBASEURL+'member/changepassword',
      data: {
        [CSRFNAME]  : CSRFHASH,
        'id'  : id,
        'new_password' : new_password,
        'confirm_password' : confirm_password,
      },
      dataType: "html",
      success: function(data){
        var response = jQuery.parseJSON(data);
        console.log(data);
        if(response.confirmation == 'error') {
          $('#error_new_password').html(response.validations['new_password']);
          $('#error_confirm_password').html(response.validations['confirm_password']);
        } else {
          $('#change').modal('hide');
          swal({
            title: "Successfully Change Password.",
            position: 'top-end',
            type: 'success',
            showConfirmButton: false,
            timer: 1600,
          });
          setTimeout(function(){ window.location.href = THEMEBASEURL+'member'; }, 1800);
        }
      }
    });
  });
});


// delete script
$(document).on('click', '.delete', function(e){
  "use strict";
  var id = $(this).attr("id");
  swal({
    title: 'Are you sure?',
    text: "It will be deleted permanently!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
    showLoaderOnConfirm: true,

    preConfirm: function() {
      return new Promise(function(resolve) {
       $.ajax({
        url: THEMEBASEURL+'member/delete',
        type: 'POST',
        data: {
          'id'       : id,
          [CSRFNAME]    : CSRFHASH,
        },
        dataType: 'json'
      })
       .done(function(response){
        swal('Deleted!', response.message, response.status);
        setTimeout(function(){ window.location.href = THEMEBASEURL+'member'; }, 2000);
      })
       .fail(function(){
        swal('Oops...', 'Something went wrong with You !', 'error');
      });
     });
    },
    allowOutsideClick: false        
  }); 
});


// status script
$(document).on('click', '.onoffswitch-small-checkbox', function(e){
  "use strict";
  var status = '';
  var id = 0;
  if($(this).prop('checked')){
    status = '1';
    id = $(this).parent().attr("id");
  } else {
    status = '0';
    id = $(this).parent().attr("id");
  }
  if((status != '' || status != null) && (id !='')) {
    $.ajax({
      type: 'POST',
      url: THEMEBASEURL+'member/status',
      data: {
        'id'      : id,
        'status'    : status,
        [CSRFNAME]   : CSRFHASH,
      },

      dataType: "html",
      success: function(data){
        var response = jQuery.parseJSON(data);
        console.log(data);
        if(response.confirmation == 'Success'){
          swal({
            title: "Successfully Updated.",
            position: 'top-end',
            type: 'success',
            showConfirmButton: false,
            timer: 1600,
          });
        } 
      }
    });
  }
});




