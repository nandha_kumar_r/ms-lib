<?php

function escapeString($val) {
    $ci = & get_instance();
    $driver = $ci->db->dbdriver;

    if( $driver == 'mysql') {
        $val = mysql_real_escape_string($val);
    } elseif($driver == 'mysqli') {
        $db = get_instance()->db->conn_id;
        $val = mysqli_real_escape_string($db, $val);
    }
    return $val;
}
if (!function_exists('dump')) {
    function dump ($var, $label = 'Dump', $echo = TRUE) {
        ob_start();
        var_dump($var);
        $output = ob_get_clean();

        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

        if ($echo == TRUE) {
            echo $output;
        }
        else {
            return $output;
        }
    }
}


if (!function_exists('dd')) {
    function dd($var="", $label = 'Dump', $echo = TRUE) {
        dump ($var, $label, $echo);
        exit;
    }
}
function pluck($array, $value, $key=NULL) {
    $returnArray = array();
    if(inicompute($array)) {
        foreach ($array as $item) {
            if($key != NULL) {
                $returnArray[$item->$key] = strtolower($value) == 'obj' ? $item : $item->$value;
            } else {
                $returnArray[] = $item->$value;
            }
        }
    }
    return $returnArray;
}

function pluck_bind($array, $value, $concatFirst, $concatLast, $key=NULL) {
    $returnArray = array();
    if(inicompute($array)) {
        foreach ($array as $item) {
            if($key != NULL) {
                $returnArray[$item->$key] = $concatFirst.$item->$value.$concatLast;
            } else {
                if($value!=NULL) {
                    $returnArray[] = $concatFirst.$item->$value.$concatLast;
                } else {
                    $returnArray[] = $concatFirst.$item.$concatLast;
                }
            }
        }
    }

    return $returnArray;
}

function pluck_multi_array($arrays, $val, $key = NULL) {
    $retArray = array();
    if(inicompute($arrays)) {
        $i = 0;
        foreach ($arrays as $array) {
            if(!empty($key)) {
                if(strtolower($val) == 'obj') {
                    $retArray[$array->$key][] = $array;
                } else {
                    $retArray[$array->$key][] = $array->$val;
                }
            } else {
                if(strtolower($val) == 'obj') {
                    $retArray[$i][] = $array;
                } else {
                    $retArray[$i][] = $array->$val;
                }
                $i++;
            }
        }
    }
    return $retArray;
}

function pluck_multi_array_key($arrays, $val, $fstKey = NULL, $sndKey = NULL) {
    $retArray = array();
    if(inicompute($arrays)) {
        $i = 0;
        foreach ($arrays as $array) {
            if(!empty($fstKey)) {
                if(strtolower($val) == 'obj') {
                    if(!empty($sndKey)) {
                        $retArray[$array->$fstKey][$array->$sndKey] = $array;
                    } else {
                        $retArray[$array->$fstKey][] = $array;
                    }
                } else {
                    if(!empty($sndKey)) {
                        $retArray[$array->$fstKey][$array->$sndKey] = $array->$val;
                    } else {
                        $retArray[$array->$fstKey][] = $array->$val;
                    }

                }
            } else {
                if(strtolower($val) == 'obj') {
                    if(!empty($sndKey)) {
                        $retArray[$i][$array->$sndKey] = $array;
                    } else {
                        $retArray[$i][] = $array;
                    }
                } else {
                    if(!empty($sndKey)) {
                        $retArray[$i][$array->$sndKey] = $array->$val;
                    } else {
                        $retArray[$i][] = $array->$val;
                    }
                }
                $i++;
            }
        }
    }
    return $retArray;
}


function namesorting($string, $len = 14) {
    $return = $string;
    if(isset($string) && $len) {
        if(strlen($string) >  $len) {
            $return = substr($string, 0,$len-2).'..';
        } else {
            $return = $string;
        }
    }

    return $return;
}

function spClean($string) {
    $string = strtolower($string);
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    function sentenceMap($string, $numberOFWord, $startTag, $closeTag) {
        $exp = explode(' ', $string);
        $len = 0;
        $expEnd = end($exp);
        $f = TRUE;
        $stringWarp = '';
        foreach ($exp as $key => $sn) {
            $len += strlen($sn);
            $len++;
            
            if($len >= $numberOFWord) {
                if($f) {
                    $stringWarp .= $startTag;
                    $f = FALSE; 
                }
            }

            $stringWarp .= $sn.' ';

            if($sn == $expEnd) {
                if($f == FALSE) {
                    $stringWarp .= $closeTag;
                }
                return $stringWarp;
            }
        }
    }

    function xssRemove($data) {
        $string = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $data);
        return $string;
    }

    function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
            switch ($num % 10) {
                case 1:  return $num.'st';
                case 2:  return $num.'nd';
                case 3:  return $num.'rd';
            }
        }
        return $num.'th';
    }

    function get_month_and_year_using_two_date($startdate, $enddate) {
        $start    = new DateTime($startdate);
        $start->modify('first day of this month');
        $end      = new DateTime($enddate);
        $end->modify('first day of next month');        
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);

        $monthAndYear = [];
        if(inicompute($period)) {
            foreach ($period as $dt) {
                $monthAndYear[ $dt->format("Y")][] =  $dt->format("m");
            }
        }
        return $monthAndYear;
    }


    function lzero($num) {
        $numPadded = sprintf("%02d", $num);
        return $numPadded;    
    }


    function btn_view_show($uri, $name) {
        return anchor($uri, "<i class='fa fa-check-square-o'></i>", "class='btn btn-success btn-xs mrg' title='".$name."' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");
    }

    function btn_edit_show($uri, $name) {
        return anchor($uri, "<i class='fa fa-edit'></i>", "class='btn btn-warning btn-xs mrg' data-placement='top' title='".$name."' data-toggle='tooltip' data-original-title='".$name."'");
    }

    function btn_complete($uri, $name) {
        return anchor($uri, "<i class='fa fa-check'></i>", "class='btn btn-info btn-xs mrg' title='".$name."' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");
    }

    function btn_delete_show($uri, $name) {
        return anchor($uri, "<i class='fa fa-trash-o'></i>",
            array(
                'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",
                'class' => 'btn btn-danger btn-xs mrg',
                'data-placement' => 'top',
                'data-toggle' => 'tooltip',
                'data-original-title' => $name,
                'title'=> $name
                )
            );
    }

    function menu_treeview_show($array, $menulink, $text1, $text2) {
        if(inicompute($array)) {
            $retArray = [];
            foreach($array as $menu) {
                $retArray[] = $menu['menulink'];
            }
            if(in_array($menulink, $retArray)) {
                return $text1;
            } else {
                return $text2;
            }
        } else {
            return "";
        }
    }

    function profile_img($imagename='default.png' , $path= 'uploads/users/') {
        $filepath = $path.$imagename;

        if(file_exists($filepath)) {
            $imgurl = base_url('uploads/users/'.$imagename);
        } else {
            $imgurl = base_url('uploads/users/default.png');
        }
        return $imgurl;
    }

    function member_img($imagename='default.png' , $path= 'uploads/member/') {
         $filepath = $path.$imagename;
        if(file_exists($filepath)){
            $imgurl = base_url('uploads/member/'.$imagename);
        } else {
            $imgurl = base_url('uploads/member/default.png');
        }
        return $imgurl;
    }



    function book_img($imagename='default.png' , $path= 'uploads/book/') {
        $filepath = $path.$imagename;
        if(file_exists($filepath)) {
            $imgurl = base_url('uploads/book/'.$imagename);
        } else {
            $imgurl = base_url('uploads/book/default.png');
        }
        return $imgurl;
    }



    function profile_bg($imagename='userprofilebg.jpg' , $path= 'uploads/default/') {
        $filepath = $path.$imagename;

        if(file_exists($filepath)) {
            $imgurl = base_url($filepath);
        } else {
            $imgurl = base_url('uploads/default/userprofilebg.jpg');
        }
        return $imgurl;
    }

    function app_date($date) {
        if($date != '') {
            return date('d M Y', strtotime($date));
        } else {
            return '';
        }
    }

    function app_time($time, $default = '') {
        if($time) {
            return date('g:i a', strtotime($time));
        } else {
            return $default;
        }
    }

    function btn_download_file($uri, $name, $lang) {
        return anchor($uri, $name, "class='btn btn-success btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$lang."'");
    }

    function btn_sm_global($uri, $name, $icon, $color = null) {
        if(!$color) {
            $color = "btn-primary";
        }
        return anchor($uri, "<i class='".$icon."'></i>", "class='btn ".$color." btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");
    }

    function btn_md_global($uri, $name, $icon, $class = null) {
        if(!$class) {
            $class = "btn-primary";
        }
        return anchor($uri, $icon, "class='".$class."' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");
    }

    function get_day_using_two_date($fromdate, $todate) {
        $datetime1 = new DateTime($fromdate);
        $datetime2 = new DateTime($todate);
        $interval = $datetime1->diff($datetime2);
        $getday = $interval->format('%a');
        $getday++;

        $array = [];
        for ($i = 1; $i  <= $getday ; $i ++) {
            if($i == 1) {
                $strtotime = strtotime($fromdate);
                $fromdate = $strtotime;
            } else {
                $strtotime = ($fromdate + (60*60*24));
                $fromdate = $strtotime;
            }
            $array[] = date('d-m-Y', $strtotime);
        }
        return $array;
    }


    function view_btn($permission,$link) {
        if(permissionChecker($permission)){
            return "<button id='".$link."' data-toggle='modal' data-target='#details' title='Details' class='btn btn-success btn-xs details'><i class='fa fa-eye'></i></button>";
        }
        return '';
    }

    function reply_btn($permission,$link) {
        if(permissionChecker($permission)){
            return "<button id='".$link."' data-toggle='modal' data-target='#reply' title='Reply' class='btn btn-info btn-xs reply'><i class='fa fa-reply'></i></button>";
        }
        return '';
    }




    function add_btn($permission,$link) {
        if(permissionChecker($permission)){
            return  "<button id='".$link."' data-toggle='modal' data-target='#insert' title='Insert' class='btn btn-inline btn-custom btn-md'><i class='fa fa-plus'></i>&nbsp;".$link."</button>";
        }
        return '';
    }

    function add_circulation_btn($permission,$link) {
        if(permissionChecker($permission)){
            return  "<button id='".$link."' data-toggle='modal' data-target='#search' title='Issued & Returned' class='btn btn-inline btn-custom btn-md'><i class='fa fa-retweet'></i>&nbsp;".$link."</button>";
        }
        return '';
    }

    function edit_btn($permission,$link){
        if(permissionChecker($permission)){
            return "<button id='".$link."' data-toggle='modal' data-target='#update' title='Update' class='btn btn-info btn-xs update'><i class='fa fa-edit'></i></button>";;
        }
        return '';
    }
    function delete_btn($permission,$link){
        if(permissionChecker($permission)){
            return "<button id='".$link."' class='btn btn-danger btn-xs delete'>&nbsp;<i class='fa fa-trash'></i></button>";
        }
        return '';
    }

function changepassword_btn($permission,$link){
    if(permissionChecker($permission)){
        return "<button type='button' class='btn btn-xs btn-info changeUserPassword' id='".$link."' title='Change Password' data-toggle='modal' data-target='#change'>&nbsp;<i class='fa fa-key'></i></button>";
    }
    return '';
}

function barcode($permission,$link) {
    if(permissionChecker($permission)){
        return  "<button id='".$link."' onclick='barcode(".$link.")' data-toggle='modal' data-target='#barcode' title='Barcode' class='btn btn-default btn-xs barcode'><i class='icofont-barcode' style='font-size: 14px;''></i></button>";
    }
    return '';
}

function payment_btn($permission,$link,$total ='null',$paid ='null',$code='null') {
   if(permissionChecker($permission)){ 
        return  "<button id='".$link."' data-code='".$code."' data-totalamount='".$total."' data-paidamount='".$paid."' data-toggle='modal' data-target='#payment' title='Payment' class='btn btn-info btn-xs payment'><i class='icofont-bill' style='font-size: 14px;''></i></button>";
    }
    return '';
}


function permissionChecker($data) {
    $CI = & get_instance();
    // $sessionPermission = $CI->session->userdata["module"][$data];

    $sessionPermission = $CI->session->userdata["module"];
    if(isset($sessionPermission[$data]) == 'YES'){
        return true;
    }
    return false;
}



function settings() {
    $CI =& get_instance();
    $array = array();
    $query = $CI->db->get('msit_tb_settings');
    if(inicompute($query))
    {
        foreach ($query->result() as $row)
        {
            $array[$row->field_option] = $row->value;
        }
    }
    // dd($array);
    return(object)$array;
}


function categorycode(){
    $CI =& get_instance();
    $query = $CI->db->select_max('categories_code')->get('msit_tb_categories');
    if (inicompute($query->row()->categories_code)){
        $categories_code = substr($query->row()->categories_code, 2);
        echo $categories_code+1;
    }else{
        echo '1001';
    }
}

function membercode(){
    $CI =& get_instance();
    $query = $CI->db->select_max('member_code')->get('msit_tb_member');
    if (inicompute($query->row()->member_code)){
        $member_code = substr($query->row()->member_code, 2);
        echo $member_code+1;
    }else{
        echo '1001';
    }
}


function circulationcode(){
    $CI =& get_instance();
    $query = $CI->db->select_max('circulation_code')->get('msit_tb_circulation');
    if (inicompute($query->row()->circulation_code)){
        $circulation_code = $query->row()->circulation_code;
        return $circulation_code+1;
    }else{
        return '1001';
    }
}

function membershipcode(){
    $CI =& get_instance();
    $query = $CI->db->select_max('membership_code')->get('msit_tb_membership');
    if (inicompute($query->row()->membership_code)){
        $membership_code = substr($query->row()->membership_code, 2);
        echo $membership_code+1;
    }else{
        echo '1001';
    }
}



if (!function_exists('inicompute')){
    function inicompute($array)
    {
        if (is_object($array) ) {
            if (count(get_object_vars($array)) ) {
                return count(get_object_vars($array));
            }
            return 0;
        } elseif(is_array($array)){
            if(count($array)) {
                return count($array);
            }
            return 0;
        } elseif(is_string($array)){
            return 1;
        } elseif (is_null($array)){
            return 0;
        } elseif (is_int($array)){
            return (int) $array;
        } else {
            return count($array);
        }
    }
}


function jsStack($array = []){
    $object = & get_instance();
    $jsList = ['THEMEBASEURL' => base_url('/'), 'CSRFNAME' => $object->security->get_csrf_token_name(), 'CSRFHASH' => $object->security->get_csrf_hash()];
    if(array_keys($array) !== range(0, inicompute($array) - 1)){
        $jsList = array_merge($jsList, $array);
    } else {
        echo "The array is not associative \n";
    }

    if(inicompute($jsList)){
        foreach ($jsList as $jsKey => $js) {
            if(is_numeric($js)) {
                echo 'const '.escapeString($jsKey) ." = ".escapeString($js)."; ";
            } else {
                echo 'const '.escapeString($jsKey) ." = '".escapeString($js)."'; ";
            }
        }
    }
}



// convart number to word
function convert_number_to_words($number) {

  $hyphen      = '-';
  $conjunction = ' ';
  $separator   = ', ';
  $negative    = 'negative';
  $decimal     = ' point';
  $dictionary  = array(
    0 => 'Zero',
    1 => 'One',
    2 => 'Two',
    3 => 'Three',
    4 => 'Fore',
    5 => 'Five',
    6 => 'Six',
    7 => 'Seven',
    8 => 'Eight',
    9 => 'Nine',
    10 => 'Ten',
    11 => 'Eleven',
    12 => 'Twelve',
    13 => 'Thirteen',
    14 => 'Fourteen',
    15 => 'Fifteen',
    16 => 'Sixteen',
    17 => 'Seventeen',
    18 => 'Eighteen',
    19 => 'Nineteen',
    20 => 'Twenty',
    27 => 'Twenty seven',
    30 => 'Thirty',
    40 => 'Forty',
    50 => 'Fifty',
    60 => 'Sixty',
    70 => 'Seventy',
    80 => 'Eighty',
    90 => 'Ninety',
    100 => 'Hundred',
    1000 => 'Thousands',
    1000000 => 'Millions',
    1000000000 => 'Billions',
    1000000000000 => 'Trillions',
    // 1000000000000000    => 'quadrillion',
    // 1000000000000000000 => 'quintillion'
    );

if (!is_numeric($number)) {
  return false;
}

if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
  trigger_error(
    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
    E_USER_WARNING
    );
  return false;
}

if ($number < 0) {
  return $negative . convert_number_to_words(abs($number));
}

$string = $fraction = null;

if (strpos($number, '.') !== false) {
  list($number, $fraction) = explode('.', $number);
}

switch (true) {
  case $number < 21:
  $string = $dictionary[$number];
  break;
  case $number < 100:
  $tens   = ((int) ($number / 10)) * 10;
  $units  = $number % 10;
  $string = $dictionary[$tens];
  if ($units) {
    $string .= $hyphen . $dictionary[$units];
  }
  break;
  case $number < 1000:
  $hundreds  = $number / 100;
  $remainder = $number % 100;
  $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
  if ($remainder) {
    $string .= $conjunction . convert_number_to_words($remainder);
  }
  break;
  default:
  $baseUnit = pow(1000, floor(log($number, 1000)));
  $numBaseUnits = (int) ($number / $baseUnit);
  $remainder = $number % $baseUnit;
  $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
  if ($remainder) {
    $string .= $remainder < 100 ? $conjunction : $separator;
    $string .= convert_number_to_words($remainder);
  }
  break;
}

if (null !== $fraction && is_numeric($fraction)) {
  $string .= $decimal;
  $words = array();
  foreach (str_split((string) $fraction) as $number) {
    $words[] = $dictionary[$number];
  }
  $string .= implode(' ', $words);
}

return $string;
}


// download export demo file
    function download_file() {
        $fileurl = base_url('uploads/download/booklist.csv');
        return $fileurl;
    }