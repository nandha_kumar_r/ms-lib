<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissionmodule_m extends MY_Model {

	protected $_table_name  = 'msit_tb_permissionmodule';
	protected $_primary_key = 'permissionmoduleID';
	protected $_order_by    = "permissionmoduleID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_permissionmodule($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_permissionmodule($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_permissionmodule($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_permissionmodule($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_permissionmodule($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_permissionmodule($id){
		parent::delete($id);
	}

}
