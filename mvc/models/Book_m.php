<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book_m extends MY_Model {

	protected $_table_name  = 'msit_tb_book';
	protected $_primary_key = 'bookID';
	protected $_order_by    = "bookID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_book($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_book($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_book($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_book($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_book($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_book($id){
		parent::delete($id);
		return TRUE;
	}

	public function hash($string) {
		return parent::hash($string);
	}

	public function insert_batch($array){
		$error = parent::insert_batch($array);
		return TRUE; 
	}


	// export csv demo file 

	public function get_export_book_list(){
	  $this->db->select("categoriesID, categories_code, categories_name");
	  $this->db->from('msit_tb_categories');
	  return $this->db->get();
	}

	public function get_export_writer_list(){
	  $this->db->select("writerID, writer_name");
	  $this->db->from('msit_tb_writer');
	  return $this->db->get();
	}

	public function get_export_publication_list(){
	  $this->db->select("publicationID, publication_name");
	  $this->db->from('msit_tb_publication');
	  return $this->db->get();
	}



}
