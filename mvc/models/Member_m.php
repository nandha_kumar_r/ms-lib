<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_m extends MY_Model {

	protected $_table_name  = 'msit_tb_member';
	protected $_primary_key = 'memberID';
	protected $_order_by    = "memberID desc";

	function __construct() {
		parent::__construct();
	}

	public function get_member($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_member($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_member($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_member($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_member($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_member($id){
		parent::delete($id);
		return TRUE;
	}
}
