<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions_m extends MY_Model{

	protected $_table_name  = 'msit_tb_permissions';
	protected $_primary_key = 'permissionsID';
	protected $_order_by    = "permissionsID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_permissions($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_permissions($wherearray=NULL, $array=NULL, $single=FALSE) {
		return parent::get_order_by($wherearray, $array, $single);
	}

	public function get_single_permissions($wherearray=NULL, $array=NULL, $single=TRUE) {
		return parent::get_single($wherearray, $array, $single);
	}

	public function insert_permissions($array) {
		return parent::insert($array);
	}

	public function update_permissions($data, $id = NULL) {
		return parent::update($data, $id);
	}

	public function delete_permissions($id){
		return parent::delete($id);
	}

	public function delete_permissions_by_usertypeID($usertypeID) {
		if((int)$usertypeID) {
			$this->db->where('usertypeID', $usertypeID);
			return $this->db->delete($this->_table_name);
		} else {
			return FALSE;
		}
	}

	public function insert_batch_permissions($array) {
		return parent::insert_batch($array);
	} 

	public function get_permissions_with_permissionlog_by_usertypeID($usertypeID){
		$this->db->select('*');
		$this->db->from('msit_tb_permissions');
		$this->db->join('msit_tb_permissionlog','msit_tb_permissions.permissionlogID = msit_tb_permissionlog.permissionlogID');
		$this->db->where('msit_tb_permissions.usertypeID', $usertypeID);
		return $this->db->get()->result();
	}

	public function get_all_permissions(){
		$query = $this->db->get('msit_tb_permissionlog');
		return $query->result();
	}

}
