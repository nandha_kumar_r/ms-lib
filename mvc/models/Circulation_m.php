<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Circulation_m extends MY_Model {

	protected $_table_name  = 'msit_tb_circulation';
	protected $_primary_key = 'circulationID';
	protected $_order_by    = "circulationID desc";

	function __construct() {
		parent::__construct();
	}

	public function get_circulation($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_circulation($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_circulation($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_circulation($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_circulation($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_circulation($id){
		parent::delete($id);
		return TRUE;
	}


	// for custom

	public function get_search($value){
		$this->db->like('member_name',$value);
		$this->db->or_like('member_code',$value);
		$query = $this->db->get_where('msit_tb_member',array('member_status' => '1'));
		return $query->result();
	}
}
