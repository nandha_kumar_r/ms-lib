<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookrequest_m extends MY_Model{

	protected $_table_name  = 'msit_tb_bookrequest';
	protected $_primary_key = 'bookrequestID';
	protected $_order_by    = "bookrequestID desc";

	function __construct() {
		parent::__construct();
	}

	public function get_bookrequest($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_bookrequest($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_bookrequest($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_bookrequest($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_bookrequest($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_bookrequest($id){
		parent::delete($id);
		return TRUE;
	}
}
