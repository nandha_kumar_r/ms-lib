<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_m extends MY_Model {

	protected $_table_name  = 'msit_tb_categories';
	protected $_primary_key = 'categoriesID';
	protected $_order_by    = "categoriesID desc";

	function __construct() {
		parent::__construct();
	}

	public function get_categories($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_categories($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_categories($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_categories($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_categories($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_categories($id){
		parent::delete($id);
		return TRUE;
	}
}
