<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Writer_m extends MY_Model {

	protected $_table_name  = 'msit_tb_writer';
	protected $_primary_key = 'writerID';
	protected $_order_by    = "writerID desc";

	function __construct() {
		parent::__construct();
	}

	public function get_writer($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_writer($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_writer($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_writer($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_writer($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_writer($id){
		parent::delete($id);
		return TRUE;
	}
}
