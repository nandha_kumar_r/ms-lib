<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usertype_m extends MY_Model {

	protected $_table_name  = 'msit_tb_usertype';
	protected $_primary_key = 'usertypeID';
	protected $_order_by    = "usertypeID asc";

	function __construct() {
		parent::__construct();
	}

	public function get_usertype($array=NULL, $single=FALSE) {
		return parent::get($array, $single);
	}

	public function get_order_by_usertype($warray=NULL, $array=NULL, $single=FALSE) {
		$query = parent::get_order_by($warray, $array, $single);
		return $query;
	}

	public function get_single_usertype($warray=NULL, $array=NULL, $single=TRUE) {
		$query = parent::get_single($warray, $array, $single);
		return $query;
	}

	public function insert_usertype($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	public function update_usertype($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_usertype($id){
		parent::delete($id);
	}

}
