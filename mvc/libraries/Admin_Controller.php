<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('menu_m');
		$this->load->model('permissions_m');
		$this->load->model('usertype_m');
		date_default_timezone_set(settings()->time_zone);
		$lang = settings()->language;
		$this->lang->load('topbar', $lang);
		
		if($this->session->userdata('usertypeID') > 0) {
			$this->data['loginrole'] = $this->usertype_m->get_single_usertype(array('usertypeID' => $this->session->userdata('usertypeID'))); 
		} else {
			$this->data['loginrole'] = [];
		}

		$this->data["jsmanager"] = [];
		
		$this->data['activemenu'] = $this->uri->segment(1);

		$exception_uris = array(
			'login/index',
			'login/forgot',
			'login/reset',
			'login/validation',
			'login/logout'
		);

		if(in_array(uri_string(), $exception_uris) == FALSE) {
			$logged = $this->session->userdata('loggedin');
			if($logged == FALSE) {
				redirect('login/index','refresh');
			}
		}
		$this->data['sidebarmenus'] = $this->sidebar_menu();

		$this->set_user_permission();

	}

	private function sidebar_menu() {
		$usertypeID = $this->session->userdata('usertypeID');
		$permissionsArray = pluck($this->permissions_m->get_permissions_with_permissionlog_by_usertypeID($usertypeID),'obj','permissionlogID');
		$menupermArray = [];
		$menupermArray[] = '#';
		if(count($permissionsArray)) {
			foreach ($permissionsArray as $permission) {
				if(!strpos($permission->name, '_')) {
					$menupermArray[] = $permission->name;
				}
			}
		}

		$menus = $this->menu_m->get_where_in_menu_by_menulink($menupermArray ,'menulink', array('status'=>1));

		$menuArray = [];
		if(count($menus)) {
			foreach($menus as $menu) {
				if($menu->parentmenuID == 0) {
					if(!isset($menuArray[$menu->menuID])) {
						$menuArray[$menu->menuID] = (array)$menu;
					} else {
						$menuArray[$menu->menuID] = array_merge((array)$menu, $menuArray[$menu->menuID]);
					}
				} else {
					if(!isset($menuArray[$menu->parentmenuID]['child'])) {
						$menuArray[$menu->parentmenuID]['child'] = array();
					}
					$menuArray[$menu->parentmenuID]['child'][$menu->menuID] = (array)$menu;
				}
			}
		}
		return $menuArray;
	}

	public function set_user_permission() {
		$permissionsArray = $this->session->userdata;
		// dd($permissionsArray);
		if(!isset($permissionsArray['get_permission'])) {
			$usertypeID = $this->session->userdata('usertypeID');
			if((int)$usertypeID) {
				if($this->session->userdata('usertypeID') == 1 && $this->session->userdata('userID') == 1) {
					if(isset($permissionsArray['loggedin']) && !isset($permissionsArray['get_permission'])) {
						$permissions = $this->permissions_m->get_all_permissions();
						$setPermissionArray = [];
						$setPermissionArray['module'] = [];
						if(count($permissions)) {
							foreach ($permissions as $permission) {
								$setPermissionArray['module'][$permission->name] = 'YES';
							}
						}
						if(ENVIRONMENT != 'production') {
							$setPermissionArray['module']['permissionlog'] = 'YES';
							$setPermissionArray['module']['permissionlog_add'] = 'YES';
							$setPermissionArray['module']['permissionlog_edit'] = 'YES';
							$setPermissionArray['module']['permissionlog_delete'] = 'YES';
							$setPermissionArray['module']['permissionmodule'] = 'YES';
							$setPermissionArray['module']['permissionmodule_add'] = 'YES';
							$setPermissionArray['module']['permissionmodule_edit'] = 'YES';
							$setPermissionArray['module']['permissionmodule_delete'] = 'YES';
							$setPermissionArray['module']['menu'] = 'YES';
							$setPermissionArray['module']['menu_add'] = 'YES';
							$setPermissionArray['module']['menu_edit'] = 'YES';
							$setPermissionArray['module']['menu_delete'] = 'YES';
						}
						$setPermissionArray['get_permission'] = 'YES';
						$this->session->set_userdata($setPermissionArray);
					}
					
				}
				else{
					if(isset($permissionsArray['loggedin']) && !isset($permissionsArray['get_permission'])) {
						$permissions = pluck($this->permissions_m->get_permissions_with_permissionlog_by_usertypeID($usertypeID),'obj','permissionlogID');
						$setPermissionArray = [];
						$setPermissionArray['module'] = [];
						if(count($permissions)) {
							foreach ($permissions as $permission) {
								$setPermissionArray['module'][$permission->name] = 'YES';
							}
						}

						$setPermissionArray['get_permission'] = 'YES';
						$this->session->set_userdata($setPermissionArray);
					}
				}
				
			}

		}

		$module = trim($this->uri->segment(1));
		$action = trim($this->uri->segment(2));
		$permission = '';
		if($action == 'index' || $action == false) {
			$permission = $module;
		} else {
			$permission = $module.'_'.$action;
		}

		$exceptURI  = array('','dashboard','dashboard_notfound','profile','profile_index','login_logout','login_index','login_forgot','login_reset','login_validation','login','permissions_save');
		$permissionModuleArray = isset($this->session->userdata['module']) ? $this->session->userdata['module'] : '';
		if(isset($permissionModuleArray[$permission])) {
			if($permissionModuleArray[$permission] != 'YES') {
				redirect('dashboard/notfound');
			}
		} else {
			if(!in_array($permission, $exceptURI)) {
				redirect('dashboard/notfound');
			}
		}
	}
}
