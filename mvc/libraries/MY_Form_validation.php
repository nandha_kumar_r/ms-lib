<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

	function __construct() {
		parent::__construct();
		$this->CI = & get_instance();
	}

	public function required_no_zero($data) {
		if($data != '') {
			if($data == '0') {
				$this->CI->form_validation->set_message('required_no_zero','The {field} field is required.');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}

	public function valid_date($date) {
		if($date) {
			if(strlen($date) != 10) {
				$this->CI->form_validation->set_message("valid_date", "The %s is not valid dd-mm-yyyy.");
		     	return FALSE;
			} else {
		   		$arr = explode("-", $date);
		        $dd = $arr[0];
		        $mm = $arr[1];
		        $yyyy = $arr[2];
		      	if(checkdate($mm, $dd, $yyyy)) {
		      		return TRUE;
		      	} else {
		      		$this->CI->form_validation->set_message("valid_date", "The %s is not valid dd-mm-yyyy.");
		     		return FALSE;
		      	}
		    }
		} else {
			return TRUE;
		}
	}

	public function valid_username($username) {
		if($username) {
			$username_pattern = "/^[a-z\d]{4,60}$/";
			$check = count(explode(' ', $username));
			if($check > 1) {
				$this->CI->form_validation->set_message("valid_username", 'Please remove white space and provide valid username.');
	     		return FALSE;
			} elseif (preg_match($username_pattern, $username)) {
				return TRUE;
			} else {
				$this->CI->form_validation->set_message("valid_username", 'Please Provide valid username.');
	     		return FALSE;
			}
		} else {
			return TRUE;
		}
	}

}