<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usertype extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('usertype_m');
		$lang = settings()->language;
		$this->lang->load('usertype', $lang);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'usertype',
				'label'=> 'Usertype',
				'rules'=> 'trim|required|min_length[2]|max_length[30]',
				)
			);
		return $rules;
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css'
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/usertype.js'
				)
			);
		if ($this->session->userdata('usertypeID') == '1') {
			$usertypes = $this->usertype_m->get_usertype(array('usertypeID','usertype'));
		}else {
			$usertypes = $this->usertype_m->get_order_by_usertype(array('usertypeID !=' => '1','usertype'));
		}


		$this->data['usertypes'] = $usertypes;
		$this->data['title'] = 'User Role';
		$this->data["subview"] = "usertype/index";
		$this->load->view('_main_layout', $this->data);
	}

	public function add(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {

				$array = []; 
				$array['usertype'] = $this->input->post('usertype'); 
				$array['create_date'] = date('Y-m-d H:i:s'); 
				$array['create_userID'] = $this->session->userdata('userID'); 
				$array['create_usertypeID'] = $this->session->userdata('usertypeID');
				$array['modify_date'] = date('Y-m-d H:i:s'); 
				$array['modify_userID'] = $this->session->userdata('userID'); 
				$array['modify_usertypeID'] = $this->session->userdata('usertypeID');
				$this->usertype_m->insert_usertype($array);
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		} else {
			redirect(base_url('usertype'));
		}
	}


	public function edit(){
		if($_POST) {
			if ($this->input->post('retrive') == '1') {
				$usertypeID = escapeString($this->input->post('id'));
				if((int)$usertypeID){
					$get_data = $this->usertype_m->get_single_usertype($usertypeID);
					if(inicompute($get_data)){
						$json = array(
							"confirmation" => 'Success',
							'id' =>$get_data->usertypeID, 
							'usertype' => $get_data->usertype,
							);
						header("Content-Type: application/json", true);
						echo json_encode($json);
						exit;
					} else {
						$json = array("confirmation" => 'error');
						header("Content-Type: application/json", true);
						echo json_encode($json);
					}
				}else {
					redirect(base_url('suppliers'));
				}
			} else{
				$this->form_validation->set_rules($this->rules());
				if($this->form_validation->run() == FALSE) {
					$validation_errors = $this->form_validation->verror_array();
					$json = array("confirmation" => 'error', 'validations' => $validation_errors);
					header("Content-Type: application/json", true);
					echo json_encode($json);
					exit;
				} else {
					$array = []; 
					$array['usertype'] = $this->input->post('usertype'); 
					$array['modify_date'] = date('Y-m-d H:i:s'); 
					$array['modify_userID'] = $this->session->userdata('userID'); 
					$array['modify_usertypeID'] = $this->session->userdata('usertypeID');
					$this->usertype_m->update_usertype($array, $this->input->post('id'));
					$json = array("confirmation" => 'Success');
					header("Content-Type: application/json", true);
					echo json_encode($json);
				} 
			}
		} else {
			redirect(base_url('usertype'));
		}
	}


	public function delete() {
		$usertypeID = escapeString($this->input->post('id'));
		if((int)$usertypeID){
			$this->usertype_m->delete_usertype($usertypeID);
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}

}
