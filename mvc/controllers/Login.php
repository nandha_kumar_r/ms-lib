<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('login_m');
		$this->load->model('user_m');
		$this->load->model('emailsetting_m');
		$lang = settings()->language;
		$this->lang->load('login', $lang);
	}

	public function rules() {
		$rules = array(
			array(
				'field'=> 'username',
				'label'=> $this->lang->line('login_username'),
				'rules'=> 'trim|required',
			),
			array(
				'field'=> 'password',
				'label'=> $this->lang->line('login_password'),
				'rules'=> 'trim|required',
			)
		);
		return $rules;
	}

	public function index(){
		$this->loggedCheck();
		$this->data['title'] = 'Login';
		if($_POST) {
			$rules  = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE){
				$this->data['errors'] = $this->form_validation->error_array();
				$this->load->view('login/index', $this->data);
			} else {
				$array['username'] = $this->input->post('username');
				$array['password'] = $this->user_m->hash($this->input->post('password'));
				$user = $this->login_m->get_single_login($array);
				if(inicompute($user)) {
					$sessionArray = []; 
					$sessionArray['userID']    	= $user->userID; 
					$sessionArray['name']    	= $user->name; 
					$sessionArray['username']  	= $user->username; 
					$sessionArray['usertypeID'] = $user->usertypeID; 
					$sessionArray['email']     	= $user->email; 
					$sessionArray['photo']     	= $user->photo; 
					$sessionArray['jod']     	= $user->jod; 
					$sessionArray['language']  	= settings()->language;
					$sessionArray['loggedin']  	= TRUE;
					$this->session->set_userdata($sessionArray);
					redirect('dashboard','refresh');
				} else {

					$this->data['errors'] = array('msg'=>"invalid username and password");
					$this->load->view('login/index', $this->data);
				}
			}
		} else {
			$this->load->view('login/index', $this->data);
		}
	}

	private function loggedCheck() {
		$logged = $this->session->userdata('loggedin');
		if($logged) {
			redirect('dashboard');
		}
	}

	public function forgot_validation() {
		$rules = array(
			array(
				'field'=> 'email_address',
				'label'=> $this->lang->line('forgot_email_address'),
				'rules'=> 'trim|required',
			)
		);
		return $rules;
	}

	public function forgot(){
	    $this->data['title'] = $this->lang->line('forgot_password_header');
		if($_POST){
		    $rules  = $this->forgot_validation();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE){
				$this->data['errors'] = $this->form_validation->error_array();
				$this->load->view('login/forgot_password', $this->data);
			}else{
				$array['email'] = $this->input->post('email_address');
				$get_mail = $this->login_m->get_single_login($array);
				if(inicompute($get_mail)){
				    
				$emailsetting = $this->emailsetting_m->get_emailsetting();
					if($emailsetting->email_engine == 'smtp'){
						$config = array(			
						    'protocol'  => 'smtp',
						    'smtp_host' => $emailsetting->smtp_server,
						    'smtp_port' => $emailsetting->smtp_port,
						    'smtp_user' => $emailsetting->smtp_username,
						    'smtp_pass' => $emailsetting->smtp_password,
						    'mailtype'  => 'html',
						    'charset'   => 'utf-8'
						);
					}else{
						$config['protocol'] = 'sendmail';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['charset'] = 'iso-8859-1';
                        $config['wordwrap'] = TRUE;
                        $config['mailtype'] ='html';
					}
                    $this->email->initialize($config);
                    $to = $this->input->post('email_address');
                    $subject = 'Validation Code for Reset Password';
                    
                	$code = rand('99','999999999');
					$updateArray['forgot_password'] = $code;
					$this->login_m->update_login($updateArray, $get_mail->userID);// update forgot password field
					// send mail script
					
					$emailContent = '<!DOCTYPE><html><head></head><body><table width="600px" style="border:1px solid #cccccc;margin: auto;border-spacing:0;"><tr><td style="background: #e6e4e4;padding: 15px 15px 0px 15px;color: #ff6b6b;"><h2>Reset Password</h2></td></tr><tr><td style="text-align: center;"><br>';
					$emailContent .= '<h2>Your Verification Code</h2><h3>'.$code.'</h3>';
					$emailContent .= '</td></tr><tr><td style="height:20px"></td></tr><tr><td style="background:#222D32;padding: 2%;text-align:center;font-size: 13px;"><p style="margin-top:1px;"><a href="'.base_url().'" target="_blank" style="text-decoration:none;color: #fff;">"'.base_url().'"</a></p></td></tr></table></body></html>';

                    $this->email->set_newline("\r\n");
                    $this->email->from(settings()->email,settings()->company_name);
                    $this->email->to($to);
                    $this->email->subject($subject);
                    $this->email->message($emailContent);
                    
					if($this->email->send()){
						redirect(base_url('login/validation'));
					}else{
						$this->data['errors'] = array('msg'=>"This Email address does not exist in our database !");
					    $this->load->view('login/forgot_password', $this->data);
					}
				}
				else{
					$this->data['errors'] = array('msg'=>"Invalid Email Address");
					$this->load->view('login/forgot_password', $this->data);
				}
			}
		}
		else{
		$this->load->view('login/forgot_password', $this->data);
		}
	}

	// validation part rule
		public function validation_rule() {
		$rules = array(
			array(
				'field'=> 'validation_code',
				'label'=> $this->lang->line('validation_code'),
				'rules'=> 'trim|required|callback_check_validation_code',
			)
		);
		return $rules;
	}


	public function check_validation_code($value){
		if (inicompute($this->login_m->get_order_by_reset_data($value))) {
			 return TRUE;
		}else{
			$this->form_validation->set_message('check_validation_code', $this->lang->line('check_validation_code'));
  			return FALSE;
		}
	}


	// validation part
	public function validation(){
		$this->data['userID']	= NULL;
		$this->data['title'] = $this->lang->line('validation');
		if($_POST){
			$rules  = $this->validation_rule();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE){
				$this->data['errors'] = $this->form_validation->error_array();
				$this->load->view('login/validation_process', $this->data);
			}else{
			 $validation_code 	= $this->input->post('validation_code');
			 $this->data['title'] = $this->lang->line('reset_password');
			 $this->data['userID']	= $this->login_m->get_order_by_reset_data($validation_code);
			 $this->load->view('login/validation_process', $this->data);
			 
			}
		}else{	
		$this->load->view('login/validation_process', $this->data);
		}
	}


	// set password rules
	public function set_password_rule() {
		$rules = array(
			array(
				'field'=> 'new_password',
				'label'=> $this->lang->line('new_password'),
				'rules'=> 'trim|required',
			),
		    array(
		       'field'   => 'confirm_new_password',
		       'label'   => $this->lang->line('confirm_new_password'),
		       'rules'   => 'required|matches[new_password]'
		    )
		);
		return $rules;
	}


	// reset password redirect page
	public function reset(){
		$this->data['title'] 	= $this->lang->line('reset_password');
		if($_POST) {
			$rules  = $this->set_password_rule();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE){
				$this->data['userID'] = $this->input->post('userID');
				$this->data['errors'] = $this->form_validation->error_array();
				$this->load->view('login/validation_process', $this->data);
			} else {
				$array['password'] 			= $this->user_m->hash($this->input->post('new_password'));
				$array['forgot_password'] 	= '';
				$this->login_m->update_login($array, $this->input->post('userID'));
				redirect('login');
			}
		} else {
			$this->load->view('login/validation_process', $this->data);
		}
	}



	public function logout() {
		$this->session->sess_destroy();
		redirect('login');
	}
}
