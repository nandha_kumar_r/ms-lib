<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Install extends CI_Controller {
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Library Management System
| -----------------------------------------------------
| AUTHOR:			Morning Sun IT 
| -----------------------------------------------------
| EMAIL:			info@morningsunit.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY Morning Sun IT
| -----------------------------------------------------
| WEBSITE:			http://morningsunit.com
| -----------------------------------------------------
*/
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('form');
		$this->load->helper('file');
		$lang = 'english';
		$this->lang->load('settings', $lang);
		$this->lang->load('user', $lang);
		if ($this->config->item('installed') != 'false') {
			show_404();

		}
	}

	protected function rules_database() {
		$rules = array(
			array(
				'field' => 'host',
				'label' => 'Hostname',
				'rules' => 'trim|required|max_length[255]'
				),
			array(
				'field' => 'database',
				'label' => 'database Name',
				'rules' => 'trim|required|max_length[255]|callback_database_unique'
				),
			array(
				'field' => 'user',
				'label' => 'username',
				'rules' => 'trim|required|max_length[255]'
				),
			array(
				'field' => 'password',
				'label' => 'password',
				'rules' => 'trim|max_length[255]'
				)
			);
		return $rules;
	}


	protected function rules_site() {
		$rules = array(
			array(
				'field'=> 'company_name',
				'label'=> $this->lang->line('company_name'),
				'rules'=> 'trim|required|max_length[60]',
				),
			array(
				'field'=> 'tag_line',
				'label'=> $this->lang->line('tag_line'),
				'rules'=> 'trim',
				),
			array(
				'field'=> 'type',
				'label'=> $this->lang->line('type'),
				'rules'=> 'trim',
				),
			array(
				'field'=> 'owner',
				'label'=> $this->lang->line('owner'),
				'rules'=> 'trim|required|max_length[30]',
				),
			array(
				'field'=> 'mobile_no',
				'label'=> $this->lang->line('mobile_no'),
				'rules'=> 'trim|required|is_numeric|max_length[20]',
				),
			array(
				'field'=> 'fax_no',
				'label'=> $this->lang->line('fax_no'),
				'rules'=> 'trim|is_numeric|max_length[20]',
				),
			array(
				'field'=> 'email',
				'label'=> $this->lang->line('email'),
				'rules'=> 'trim|required|valid_email',
				),

			array(
				'field'=> 'tax_no',
				'label'=> $this->lang->line('tax_no'),
				'rules'=> 'trim|numeric|max_length[32]',
				),
			array(
				'field'=> 'address',
				'label'=> $this->lang->line('address'),
				'rules'=> 'trim|required|max_length[70]',
				),
			array(
				'field'=> 'time_zone',
				'label'=> $this->lang->line('time_zone'),
				'rules'=> 'trim|required|required_no_zero',
				),
			array(
				'field'=> 'currency_code',
				'label'=> $this->lang->line('currency_code'),
				'rules'=> 'trim|required|max_length[3]',
				),
			array(
				'field'=> 'currency_symbol',
				'label'=> $this->lang->line('currency_symbol'),
				'rules'=> 'trim|required|max_length[2]',
				),
			array(
				'field'=> 'logo',
				'label'=> $this->lang->line('logo'),
				'rules'=> 'trim|max_length[200]|callback_photo_upload',
				),
			array(
				'field'=> 'user_username',
				'label'=> $this->lang->line('user_username'),
				'rules'=> 'trim|required|strtolower|callback_unique_username',
				),
			array(
				'field'=> 'user_password',
				'label'=> $this->lang->line('user_password'),
				'rules'=> 'trim|required',
				),
			array(
				'field'=> 'confirm_password',
				'label'=> $this->lang->line('user_confirm_password'),
				'rules'=> 'trim|required|matches[user_password]',
				),
			);
	return $rules;
	}

	function unique_username($value){
		if ($value == 'admin' || $value =='root'){
			$this->form_validation->set_message('unique_username','This %s is not allowed in this system!');
			return FALSE;
		}else{
			return TRUE;
		}
	}

	function index() {
		$data = array();
		$data['errors'] = array();
		$data['success'] = array();


		if (phpversion() < "5.3") {
			$data['errors'][] = 'You are running PHP old version!';
		} else {
			$phpversion = phpversion();
			$data['success'][] = ' You are running PHP '.$phpversion;
		}
		if (phpversion() < "7.3") {
			// Check Mcrypt PHP exention
			if(!extension_loaded('mcrypt')) {
				$data['errors'][] = 'Mcrypt PHP exention unloaded!';
			} else {
				$data['success'][] = 'Mcrypt PHP exention loaded!';
			}
				// Check Mysql PHP exention
			if(!extension_loaded('mysql')) {
				$data['errors'][] = 'Mysql PHP exention unloaded!';
			} else {
				$data['success'][] = 'Mysql PHP exention loaded!';
			}
		}
			// Check Mysql PHP exention
		if(!extension_loaded('mysqli')) {
			$data['errors'][] = 'Mysqli PHP exention unloaded!';
		} else {
			$data['success'][] = 'Mysqli PHP exention loaded!';
		}
			// Check MBString PHP exention
		if(!extension_loaded('mbstring')) {
			$data['errors'][] = 'MBString PHP exention unloaded!';
		} else {
			$data['success'][] = 'MBString PHP exention loaded!';
		}
			// Check GD PHP exention
		if(!extension_loaded('gd')) {
			$data['errors'][] = 'GD PHP exention unloaded!';
		} else {
			$data['success'][] = 'GD PHP exention loaded!';
		}
			// Check CURL PHP exention
		if(!extension_loaded('curl')) {
			$data['errors'][] = 'CURL PHP exention unloaded!';
		} else {
			$data['success'][] = 'CURL PHP exention loaded!';
		}
			// Check Config Path
		if (@include($this->config->config_path)) {
			@chmod($this->config->config_path, FILE_WRITE_MODE);
			if(is_really_writable($this->config->config_path) == TRUE) {
				$data['success'][] = 'Config file is writable';
			} else {
				$data['errors'][] = 'Config file is unwritable!';
			}
		} else {
			$data['errors'][] = 'Config file is unloaded';
		}

		// Check autoloaded Path
		if (@include($this->config->autoload_path)) {
			@chmod($this->config->autoload_path, FILE_WRITE_MODE);
			if(is_really_writable($this->config->autoload_path) == TRUE) {
				$data['success'][] = 'Autoload file is writable';
			} else {
				$data['errors'][] = 'Autoload file is unwritable!';
			}
		} else {
			$data['errors'][] = 'Autoload file is unloaded';
		}

		// Check Database Path
		if (@include($this->config->database_path)) {
			@chmod($this->config->database_path, FILE_WRITE_MODE);
			if (is_really_writable($this->config->database_path) === FALSE) {
				$data['errors'][] = 'database file is unwritable!';
			} else {
				$data['success'][] = 'Database file is writable';
			}

		} else {
			$data['errors'][] = 'Database file is unloaded';
		}

		$data['checkout'] = 1;
		$data['purchasekey'] = 0;
		$data['database'] = 0;
		$data['site'] = 0;
		$data['done'] = 0;
		if (inicompute($data['errors']) == 0) {
			$data["subview"] = "install/index";
			$this->load->view('_install_layout', $data);
		} else {
			$data["subview"] = "install/index";
			$this->load->view('_install_layout', $data);
		}
	}
	
	protected function rulesForPurchacekey(){
		$rules = array(
			array(
				'field' => 'purchasekey',
				'label' => 'Purchase Key',
				'rules' => 'trim|required|max_length[36]|callback_chk_puechacekey'
				)
			);
		return $rules;
	}
	
	public function chk_puechacekey(){
		$getKey = $this->input->post('purchasekey');
		if($getKey){
			if($this->verify_envato_purchase_code($getKey) == TRUE){
				$file = APPPATH.'config/purchase'.EXT;
				@chmod($file, FILE_WRITE_MODE);
				$purchase_file = file_get_contents($file);
				write_file($file, $getKey);
				return TRUE;
			}else {
				$this->form_validation->set_message('chk_puechacekey','The purchase key is invilid.');
				return FALSE;
			}
		} else {
			$this->form_validation->set_message('chk_puechacekey','The %s field is required.');
			return FALSE;
		}
	}


	function purchasekey(){
		$data['checkout'] = 1;
		$data['purchasekey'] = 1;
		$data['database'] = 0;
		$data['site'] = 0;
		$data['done'] = 0;
		if($_POST) {
			$rules = $this->rulesForPurchacekey();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$data["subview"] = "install/purchasekey";
				$this->load->view('_install_layout', $data);
			} else {
				redirect(base_url("install/database"));
			}
		} else {
			$data["subview"] = "install/purchasekey";
			$this->load->view('_install_layout', $data);
		}
	}

	function database(){
		if($this->get_purchase_code()){
			$data['checkout'] = 1;
			$data['purchasekey'] = 1;
			$data['database'] = 1;
			$data['site'] = 0;
			$data['done'] = 0;
			if($_POST) {
				$rules = $this->rules_database();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$data["subview"] = "install/database";
					$this->load->view('_install_layout', $data);
				} else {
					$host = $this->input->post('host');
					$user = $this->input->post('user');
					$password = $this->input->post('password');
					$database = $this->input->post('database');

					redirect(base_url("install/site"));
				}
			} else {
				$data["subview"] = "install/database";
				$this->load->view('_install_layout', $data);
			}
		} else {
			redirect(base_url("install/purchasekey"));
		}
	}


	public function photo_upload() {
		// $settings = $this->settings_m->get_settings_data(1);
		$new_file = "default.png";
		if($_FILES["logo"]['name'] !="") {
			$file_name = $_FILES["logo"]['name'];
			$makeRandom = hash('sha512', config_item("encryption_key"));
			$file_name_rename = $makeRandom;
			$explode = explode('.', $file_name);
			if(inicompute($explode) >= 2) {
				$new_file = $file_name_rename.'.'.end($explode);
				$config['upload_path'] = "./uploads/images";
				$config['allowed_types'] = "gif|jpg|png";
				$config['file_name'] = $new_file;
				$config['max_size'] = '1024';
				$config['max_width'] = '3000';
				$config['max_height'] = '3000';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if(!$this->upload->do_upload("logo")) {
					$this->form_validation->set_message("photoupload", $this->upload->display_errors());
					return FALSE;
				} else {
					$this->upload_data['file'] =  $this->upload->data();
					return TRUE;
				}
			} else {
				$this->form_validation->set_message("photoupload", "Invalid file");
				return FALSE;
			}
		} else {
			// if(!inicompute($settings)){
				$this->upload_data['file'] = array('file_name' => $new_file);
				return TRUE;
			// } else {
			// 	$this->upload_data['file'] = array('file_name' => $settings->logo);
			// 	return TRUE;
			// }
		}
	}

	function site(){
		if($this->get_purchase_code()) {

			$data['checkout'] = 1;
			$data['purchasekey'] = 1;
			$data['database'] = 1;
			$data['site'] = 1;
			$data['done'] = 0;
			if($_POST) {
				$this->load->database();
				$this->load->library('session');
				unset($this->db);
				$rules = $this->rules_site();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$data["subview"] = "install/site";
					$this->load->view('_install_layout', $data);
				} else {
					$this->load->helper('form');
					$this->load->helper('url');
					$this->load->model('user_m');
					$this->load->model('settings_m');

					$array = array();
					for($i=0; $i<inicompute($rules); $i++) {
						if($this->input->post($rules[$i]['field']) == false) {
							$array[$rules[$i]['field']] = '';
						} else {
							$array[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
						}
					}
					$array['logo'] = $this->upload_data['file']['file_name'];
					$array['phone'] = $array['mobile_no'];
					$array['language'] = 'English';
					$array['category_prefixe'] = 'CA';
					$array['member_prefixe'] = 'MB';
					$array['book_prefixe'] = 'BK';
					$array['membership_prefixe'] = 'MS';
					$array["modify_date"] = date('Y-m-d g:i A');

					unset($array['user_username']);
					unset($array['user_password']);
					unset($array['confirm_password']);

					$arrayUser = [];
					$arrayUser['name']    			= $this->input->post('owner');
					$arrayUser['email']   			= $this->input->post('email');
					$arrayUser['address'] 			= $this->input->post('address');
					$arrayUser['phone'] 			= $this->input->post('mobile_no');
					$arrayUser['dob'] 				= date('Y-m-d');
					$arrayUser['jod'] 				= date('Y-m-d');
					$arrayUser['photo']   			= $this->upload_data['file']['file_name'];
					$arrayUser['usertypeID']		= '1';
					$arrayUser['status']  			= '1';
					$arrayUser['username']			= $this->input->post('user_username');
					$arrayUser['password']			= $this->user_m->hash($this->input->post('user_password'));
					$arrayUser['create_date']      	= date('Y-m-d H:i:s');
					$arrayUser['create_userID']    	= '1';
					$arrayUser['create_usertypeID']	= '1';
					$arrayUser['modify_date']      	= date('Y-m-d H:i:s');
					$arrayUser['modify_userID']    	= '1';
					$arrayUser['modify_usertypeID']	= '1';

					$this->load->database();
					if ($this->settings_m->insert_or_update($array)){
						$this->load->library('session');
						$sesdata= array(
							'username'  => $this->input->post('user_username'),
							'password'  => $this->input->post('user_password'),
						);
						$this->user_m->insert_user($arrayUser);
						$this->session->set_userdata($sesdata);
						redirect(base_url("install/done"));
					}
				}
			} else {
				$data["subview"] = "install/site";
				$this->load->view('_install_layout', $data);
			}
		} else {
			redirect(base_url("install/purchasekey"));
		}
	}

	function done(){
		if($this->get_purchase_code()) {
			$data['checkout'] = 1;
			$data['purchasekey'] = 1;
			$data['database'] = 1;
			$data['site'] = 1;
			$data['done'] = 1;
			$this->load->library('session');
			if($_POST) {
				$this->config->config_update(array("installed" => 'true'));
				$this->config->autolod_update(array("libraries" => "array('database','email', 'session','form_validation','upload','ciqrcode')"));
				@chmod($this->config->database_path, FILE_READ_MODE);
				@chmod($this->config->config_path, FILE_READ_MODE);
				$this->session->sess_destroy();
				redirect(base_url('login/index'));
			} else {
				$data["subview"] = "install/done";
				$this->load->view('_install_layout', $data);
			}

		}else {
			redirect(base_url("install/purchasekey"));
		}
	}

	function database_unique() {
		error_reporting(0);
		$host = $this->input->post('host');
		$user = $this->input->post('user');
		$password = $this->input->post('password');
		$database = $this->input->post('database');
		
		$config_con['hostname'] = $host;
		$config_con['username'] = $user;
		$config_con['password'] = $password;
		$config_con['database'] = $database;
		$config_con['dbdriver'] = 'mysqli';

		$config_db['default']['hostname'] = $host;
		$config_db['default']['username'] = $user;
		$config_db['default']['password'] = $password;
		$config_db['default']['database'] = $database;
		$config_db['default']['dbdriver'] = 'mysqli';
		
		$this->config->db_config_update($config_db);
		$db_obj = $this->load->database($config_con,TRUE);
		$connected = $db_obj->initialize();
		
		if($connected == FALSE) {
			unset($this->db);
			$config_con['db_debug'] = FALSE;
			$this->form_validation->set_message('database_unique','Database Connection Failed.');
			return FALSE;
		} else {
			unset($this->db);
			$config_con['db_debug'] = FALSE;
			
			$this->load->database($config_con);
			$this->load->dbutil();
			if ($this->dbutil->database_exists($this->db->database)) {
				if ($this->db->table_exists('msit_tb_settings') == FALSE) {
					$id = uniqid();
					$encryption_key = md5("Rohit".$id);
					$this->config->config_update(array('encryption_key'=> $encryption_key));
					$this->load->model('install_m');
					$this->install_m->use_sql_string();
					return TRUE;
				}
				return TRUE;
			} else {
				$this->form_validation->set_message("database_unique", "Database Not Found.");
				return FALSE;
			}
			
		}
	}
	
	function index_validation() {
		$timezone = $this->input->post('timezone');
		@chmod($this->config->index_path, 0777);
		if (is_really_writable($this->config->index_path) === FALSE) {
			$this->form_validation->set_message("index_validation", "Index file is unwritable");
			return FALSE;
		} else {
			$file = $this->config->index_path;
			$current = file_get_contents($file);
			$current = "<?php \ndate_default_timezone_set('". $timezone ."');\n?>\n".$current;
			if(file_put_contents($file, $current)) {
				@chmod($this->config->index_path, 0644);
				return TRUE;
			}
		}
	}

	function verify_envato_purchase_code($code_to_verify) {
		$this->load->library('EnvatoPurchaseCodeVerifier');
		$access_token = 'sK1cQ4zMpdHAYWAUu0bR9LIpwzyLdv6r';
		$purchase = new EnvatoPurchaseCodeVerifier($access_token);
		// $buyer_purchase_code = filter_input(INPUT_POST, 'purchasekey', FILTER_DEFAULT);
		$buyer_purchase_code = $code_to_verify;
		if (!empty($buyer_purchase_code)){
			$verified = $purchase->verified($buyer_purchase_code);
			if ($verified == true){
				return TRUE;
			}else {
				$this->form_validation->set_message('chk_puechacekey','The purchase key is invilid.');
				return FALSE;
			}
		}else {
			$this->form_validation->set_message('chk_puechacekey','The purchase key is invilid.');
			return FALSE;
		}
	}

	function get_purchase_code(){
		$file = APPPATH.'config/purchase'.EXT;
		@chmod($file, FILE_WRITE_MODE);
		$purchase = file_get_contents($file);
		return $this->verify_envato_purchase_code($purchase);
	}
}

	/* End of file install.php */
	/* Location: ./application/controllers/install.php */
