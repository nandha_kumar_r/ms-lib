<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('report_m');
		$this->load->model('membership_m');
		$this->load->model('categories_m');
		$this->load->model('book_m');
		$this->load->model('writer_m');
		$this->load->model('member_m');
		$lang = settings()->language;
		$this->lang->load('report', $lang);
	}

	public function index(){
		$this->data['title'] = 'Report';
		$this->data["subview"] = "report/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function memberRules() {
		$rules = array(
			array(
				'field'=> 'membership',
				'label'=> $this->lang->line('reports_membership_type'),
				'rules'=> 'trim|required|max_length[60]|required_no_zero',
			)
		);
		return $rules;
	}


	public function member(){
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/custom/css/hidetable.css',
				'assets/bower_components/select2/select2.css',
				'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				'assets/pages_js/report.js',
				)
			);
		$this->data['result'] 	= array();
		$this->data['date'] 	= array();
		if($_POST){
			$rules = $this->memberRules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() != FALSE){
				$membershipID 	= $this->input->post('membership');
				$status 		= $this->input->post('member_status');

				$this->data['status'] = (!empty($status)?'Active':'Inactive');
				$this->data['membershipname'] = $this->membership_m->get_single_membership(array('membershipID' => $membershipID));
				$this->data['result'] = $this->report_m->member_report($membershipID,$status);
			} 
		}
		$this->data['title'] = 'Member Report';
		$this->data["subview"] = "report/member";
		$this->data['memberships'] = $this->membership_m->get_order_by_membership(array('membership_status' => 1));
		$this->load->view('_main_layout', $this->data);
	}

	protected function bookRules() {
		$rules = array(
			array(
				'field'=> 'categoties',
				'label'=> $this->lang->line('reports_categories_type'),
				'rules'=> 'trim|required|max_length[60]|required_no_zero',
			)
		);
		return $rules;
	}
	public function book(){
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/custom/css/hidetable.css',
				'assets/bower_components/select2/select2.css',
				'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				'assets/pages_js/report.js',
				)
			);
		$this->data['result'] 	= array();
		$this->data['date'] 	= array();
		if($_POST){
			$rules = $this->bookRules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() != FALSE){
				$categoriesID 	= $this->input->post('categoties');
				$status 		= $this->input->post('status');
				$this->data['status'] = (!empty($status)?'Active':'Inactive');
				$this->data['writer'] = pluck($this->writer_m->get_writer(array('writerID','writer_name')),'writer_name','writerID');	
				$this->data['categoriesname'] = $this->categories_m->get_single_categories(array('categoriesID' => $categoriesID));
				$this->data['result'] = $this->report_m->book_report($categoriesID,$status);
			} 
		}
		$this->data['title'] = 'Book Report';
		$this->data["subview"] = "report/book";
		$this->data['categories'] = $this->categories_m->get_order_by_categories(array('categories_status' => 1));
		$this->load->view('_main_layout', $this->data);
	}


	public function categories(){
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/custom/css/hidetable.css',
				'assets/bower_components/select2/select2.css',
				'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				'assets/pages_js/report.js',
				)
			);
		$this->data['title'] = 'Categories Report';
		$this->data["subview"] = "report/categories";
		$this->data['categories'] = $this->categories_m->get_order_by_categories(array('categories_status' => 1));
		$this->data['booklist'] = $this->book_m->get_book();
		$this->load->view('_main_layout', $this->data);
	}



	public function payment(){
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/custom/css/hidetable.css',
				'assets/bower_components/select2/select2.css',
				'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				'assets/pages_js/report.js',
				)
			);
		$this->data['result'] 	= array();
		$this->data['date'] 	= array();
		if($_POST){
			$for 	= $this->input->post('for');
			$fromdate = date('Y-m-d',strtotime($this->input->post('from_date')));
			$todate = date('Y-m-d',strtotime($this->input->post('to_date')));
			$this->data['date'] = array('from_date'=>$fromdate, 'to_date'=>$todate);
			$this->data['membername'] = pluck($this->member_m->get_member(array('memberID','member_name')),'member_name','memberID');		
			$this->data['result'] = $this->report_m->payment_report($for,$fromdate,$todate);	
		}
		$this->data['title'] = 'payment Report';
		$this->data["subview"] = "report/payment";
		$this->data['memberships'] = $this->membership_m->get_order_by_membership(array('membership_status' => 1));
		$this->load->view('_main_layout', $this->data);
	}



}
