<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('usertype_m');
		$lang = settings()->language;
		$this->lang->load('user', $lang);
	}

	public function index() {
		$userID = $this->session->userdata("userID");
		if((int)$userID) {
			$user = $this->user_m->get_single_user(array('userID' => $userID));
			if(inicompute($user)) {
				$this->data['usertypes'] = pluck($this->usertype_m->get_usertype(array('usertypeID','usertype')),'usertype','usertypeID');
				$this->data['user'] = $user;
				$this->data["subview"] = "user/view";
				$this->load->view('_main_layout', $this->data);
			} else {
				$this->data["subview"] = "_not_found";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	}
}
