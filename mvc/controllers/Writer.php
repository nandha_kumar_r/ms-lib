<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Writer extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('writer_m');
		$lang = settings()->language;
		$this->lang->load('writer', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
				),
			'js' => array(
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/writer.js',
				)
			);
		$this->data['writer'] = $this->writer_m->get_writer();
		$this->data['title'] = 'writer';
		$this->data["subview"] = "writer/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'writer_name',
				'label'=> $this->lang->line('writer_name'),
				'rules'=> 'trim|required|max_length[25]',
				),
			array(
				'field'=> 'writer_note',
				'label'=> $this->lang->line('writer_note'),
				'rules'=> 'trim|max_length[60]',
				)
			);
		return $rules;
	}

	public function add(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {

				$array = array(
					'writer_code'    => $this->input->post('writer_code'),
					'writer_name'    => $this->input->post('writer_name'),
					'writer_note'  	=> $this->input->post('writer_note'),
					'writer_status'  => '1',
					'create_date'      	=> date('Y-m-d H:i:s'),
					'create_userID'    	=> $this->session->userdata('userID'),
					'create_usertypeID'	=> $this->session->userdata('usertypeID'),
					'modify_date'      	=> date('Y-m-d H:i:s'),
					'modify_userID'    	=> $this->session->userdata('userID'),
					'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
				$this->writer_m->insert_writer($array);
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		} else {
			redirect(base_url('writer'));
		}
	}


	public function edit(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$array = array(
					'writer_name'    => $this->input->post('writer_name'),
					'writer_note'  	=> $this->input->post('writer_note'),
					'modify_date'      	=> date('Y-m-d H:i:s'),
					'modify_userID'    	=> $this->session->userdata('userID'),
					'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
					);
				$this->writer_m->update_writer($array, $this->input->post('id'));
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			} 
		}else {
			redirect(base_url('expenses'));
		}
	}

	public function retrive() {
		$writerID = $this->input->post('id');
		if((int)$writerID){
			$get_data = $this->writer_m->get_single_writer(array('writerID' => $writerID));
			if(inicompute($get_data)){
				$json = array(
					"confirmation" => 'Success',
					'id' => $get_data->writerID, 
					'writer_name' => $get_data->writer_name,
					'writer_note' => $get_data->writer_note,
					);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$json = array("confirmation" => 'error');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		}else {
			redirect(base_url('writer'));
		}
	}

	public function status(){
		$array = array(
			'writer_status'    => $this->input->post('status'),
			);
		$this->writer_m->update_writer($array, $this->input->post('id'));
		$json = array("confirmation" => 'Success');
		header("Content-Type: application/json", true);
		echo json_encode($json);
		
	}

	public function delete() {
		if($this->writer_m->delete_writer($this->input->post('id'))){
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		}
	}
}
