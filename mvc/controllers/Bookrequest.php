<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookrequest extends Admin_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('bookrequest_m');
		$this->load->model('member_m');
		$lang = settings()->language;
		$this->lang->load('bookrequest', $lang);
	}

	public function index() {
		$this->data['headerassets'] = array(
			'css' => array(
				'assets/bower_components/select2/select2.css',
				'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
				'assets/custom/css/hidetable.css',
				),
			'js' => array(
				'assets/bower_components/select2/select2.js',
				'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
				'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
				'assets/pages_js/bookrequest.js',
				)
			);
		$this->data['bookrequest'] = $this->bookrequest_m->get_bookrequest();
		$this->data['memberlist'] = $this->member_m->get_member();
		$this->data['memberID'] = pluck($this->member_m->get_member(),'member_name','memberID');
		$this->data['title'] = 'bookrequest';
		$this->data["subview"] = "bookrequest/index";
		$this->load->view('_main_layout', $this->data);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'book_name',
				'label'=> $this->lang->line('bookrequest_name'),
				'rules'=> 'trim|required|max_length[25]',
				),
			array(
				'field'=> 'writer_name',
				'label'=> $this->lang->line('bookrequest_writer_name'),
				'rules'=> 'trim|required|max_length[25]',
				),
			array(
				'field'=> 'categories',
				'label'=> $this->lang->line('bookrequest_categories'),
				'rules'=> 'trim|required|max_length[25]',
				),
			array(
				'field'=> 'edition',
				'label'=> $this->lang->line('bookrequest_edition'),
				'rules'=> 'trim|max_length[2]',
				),
			array(
				'field'=> 'memberID',
				'label'=> $this->lang->line('bookrequest_member'),
				'rules'=> 'trim|required|max_length[25]|required_no_zero',
				),
			array(
				'field'=> 'bookrequest_note',
				'label'=> $this->lang->line('bookrequest_note'),
				'rules'=> 'trim|max_length[60]',
				)
			);
		return $rules;
	}

	public function add(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$array = array(
					'bookrequest_name'    		=> $this->input->post('book_name'),
					'bookrequest_writer_name'   => $this->input->post('writer_name'),
					'bookrequest_categories'  	=> $this->input->post('categories'),
					'bookrequest_edition'  		=> $this->input->post('edition'),
					'bookrequest_memberID'  	=> $this->input->post('memberID'),
					'bookrequest_note'		  	=> $this->input->post('bookrequest_note'),
					'create_date'      			=> date('Y-m-d H:i:s'),
					'create_userID'    			=> $this->session->userdata('userID'),
					'create_usertypeID'			=> $this->session->userdata('usertypeID'),
					'modify_date'      			=> date('Y-m-d H:i:s'),
					'modify_userID'    			=> $this->session->userdata('userID'),
					'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
				);
				$this->bookrequest_m->insert_bookrequest($array);
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		} else {
			redirect(base_url('bookrequest'));
		}
	}


	public function edit(){
		if($_POST) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == FALSE) {
				$validation_errors = $this->form_validation->verror_array();
				$json = array("confirmation" => 'error', 'validations' => $validation_errors);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$array = array(
					'bookrequest_name'    		=> $this->input->post('book_name'),
					'bookrequest_writer_name'   => $this->input->post('writer_name'),
					'bookrequest_categories'  	=> $this->input->post('categories'),
					'bookrequest_edition'  		=> $this->input->post('edition'),
					'bookrequest_memberID'  	=> $this->input->post('memberID'),
					'bookrequest_note'		  	=> $this->input->post('bookrequest_note'),
					'modify_date'      			=> date('Y-m-d H:i:s'),
					'modify_userID'    			=> $this->session->userdata('userID'),
					'modify_usertypeID'			=> $this->session->userdata('usertypeID'),
					);
				$this->bookrequest_m->update_bookrequest($array, $this->input->post('id'));
				$json = array("confirmation" => 'Success');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			} 
		}else {
			redirect(base_url('expenses'));
		}
	}

	public function retrive() {
		$bookrequestID = $this->input->post('id');
		if((int)$bookrequestID){
			$get_data = $this->bookrequest_m->get_single_bookrequest(array('bookrequestID' => $bookrequestID));
			if(inicompute($get_data)){
				$json = array(
					"confirmation" => 'Success',
					'id' 				=> $get_data->bookrequestID, 
					'book_name' 		=> $get_data->bookrequest_name,
					'writer_name' 		=>$get_data->bookrequest_writer_name,
					'categories' 		=> $get_data->bookrequest_categories,
					'edition' 			=>$get_data->bookrequest_edition,
					'memberID' 			=>$get_data->bookrequest_memberID,
					'bookrequest_note' 	=>$get_data->bookrequest_note,
					);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				$json = array("confirmation" => 'error');
				header("Content-Type: application/json", true);
				echo json_encode($json);
			}
		}else {
			redirect(base_url('bookrequest'));
		}
	}

	public function status(){
		$array = array(
			'bookrequest_status'    => $this->input->post('status'),
			);
		$this->bookrequest_m->update_bookrequest($array, $this->input->post('id'));
		$json = array("confirmation" => 'Success');
		header("Content-Type: application/json", true);
		echo json_encode($json);
		
	}

	public function delete() {
		if($this->bookrequest_m->delete_bookrequest($this->input->post('id'))){
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		}
	}
}
