<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_m');
		$this->load->model('usertype_m');
		$lang = settings()->language;
		$this->lang->load('user', $lang);
	}

	protected function rules() {
		$rules = array(
			array(
				'field'=> 'name',
				'label'=> $this->lang->line('user_name'),
				'rules'=> 'trim|required|max_length[60]',
				),
			array(
				'field'=> 'dob',
				'label'=> $this->lang->line('user_dob'),
				'rules'=> 'trim|required|valid_date',
				),
			array(
				'field'=> 'gender',
				'label'=> $this->lang->line('user_gender'),
				'rules'=> 'trim|required|max_length[15]|required_no_zero',
				),
			array(
				'field'=> 'religion',
				'label'=> $this->lang->line('user_religion'),
				'rules'=> 'trim|required|max_length[30]',
				),
			array(
				'field'=> 'email',
				'label'=> $this->lang->line('user_email'),
				'rules'=> 'trim|required|max_length[60]|valid_email|callback_check_unique_email',
				),
			array(
				'field'=> 'phone',
				'label'=> $this->lang->line('user_phone'),
				'rules'=> 'trim|required|max_length[15]',
				),
			array(
				'field'=> 'address',
				'label'=> $this->lang->line('user_address'),
				'rules'=> 'trim|required|min_length[10]',
				),
			array(
				'field'=> 'jod',
				'label'=> $this->lang->line('user_jod'),
				'rules'=> 'trim|required|valid_date',
				),
			array(
				'field'=> 'photo',
				'label'=> $this->lang->line('user_photo'),
				'rules'=> 'trim|max_length[200]|callback_photo_upload',
				),
			array(
				'field'=> 'status',
				'label'=> $this->lang->line('user_status'),
				'rules'=> 'trim|required|numeric|required_no_zero',
				),
			array(
				'field'=> 'usertypeID',
				'label'=> $this->lang->line('user_role'),
				'rules'=> 'trim|required|numeric|required_no_zero',
				),
			array(
				'field'=> 'username',
				'label'=> $this->lang->line('user_username'),
				'rules'=> 'trim|required|max_length[60]|callback_check_unique_username',
				),
			array(
				'field'=> 'password',
				'label'=> $this->lang->line('user_password'),
				'rules'=> 'trim|required|max_length[128]',
				)
			);
return $rules;
}

public function index() {
	$this->data['headerassets'] = array(
		'css' => array(
			'assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
			'assets/custom/css/hidetable.css'
			),
		'js' => array(
			'assets/bower_components/datatables.net/js/jquery.dataTables.min.js',
			'assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
			'assets/custom/js/bootstrap-show-password.min.js',
			'assets/pages_js/user.js',
			)
		);
	if ($this->session->userdata('usertypeID') == '1') {
		$users = $this->user_m->get_user();
	}else {
		$users = $this->user_m->get_order_by_user(array('usertypeID !=' => '1'));
	}

	$this->data['usertypes'] = pluck($this->usertype_m->get_usertype(array('usertypeID','usertype')),'usertype','usertypeID');
	$this->data['users'] = $users;
	$this->data['title'] = 'User';
	$this->data["subview"] = "user/index";
	$this->load->view('_main_layout', $this->data);
}

public function add() {
	$this->data['headerassets'] = array(
		'css' => array(
			'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
			),
		'js' => array(
			'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
			)
		);

	if ($this->session->userdata('usertypeID') == '1') {
		$usertypes = $this->usertype_m->get_usertype(array('usertypeID','usertype'));
	}else {
		$usertypes = $this->usertype_m->get_order_by_usertype(array('usertypeID !=' => '1','usertype'));
	}


	$this->data['usertypes'] = $usertypes;
	if($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
			$this->data["subview"] = "user/add";
			$this->load->view('_main_layout', $this->data);
		} else {
			$array = [];
			$array['name']    			= $this->input->post('name');
			$array['dob']     			= date('Y-m-d',strtotime($this->input->post('dob')));
			$array['gender']  			= $this->input->post('gender');
			$array['religion']			= $this->input->post('religion');
			$array['email']   			= $this->input->post('email');
			$array['phone']   			= $this->input->post('phone');
			$array['address'] 			= $this->input->post('address');
			$array['jod']     			= date('Y-m-d',strtotime($this->input->post('jod')));
			$array['photo']   			= $this->upload_data['photo']['file_name'];;
			$array['usertypeID']		= $this->input->post('usertypeID');
			$array['status']  			= $this->input->post('status');
			$array['username']			= $this->input->post('username');
			$array['password']			= $this->user_m->hash($this->input->post('password'));
			$array['create_date']      	= date('Y-m-d H:i:s');
			$array['create_userID']    	= $this->session->userdata('userID');
			$array['create_usertypeID']	= $this->session->userdata('usertypeID');
			$array['modify_date']      	= date('Y-m-d H:i:s');
			$array['modify_userID']    	= $this->session->userdata('userID');
			$array['modify_usertypeID']	= $this->session->userdata('usertypeID');

			$this->user_m->insert_user($array);
			$this->session->set_flashdata('message','Success');
			redirect('user/index');
		}
	} else {
		$this->data['title'] = 'User Add';
		$this->data["subview"] = "user/add";
		$this->load->view('_main_layout', $this->data);
	}
}

public function edit() {
	$userID = htmlentities(escapeString($this->uri->segment(3)));
	if((int)$userID) {
		$user = $this->user_m->get_single_user(array('userID' => $userID));
		if(inicompute($user)) {
			$this->data['headerassets'] = array(
				'css' => array(
					'assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
					),
				'js' => array(
					'assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
					)
				);

			if ($this->session->userdata('usertypeID') == '1') {
				$usertypes = $this->usertype_m->get_usertype(array('usertypeID','usertype'));
			}else {
				$usertypes = $this->usertype_m->get_order_by_usertype(array('usertypeID !=' => '1','usertype'));
			}


			$this->data['usertypes'] = $usertypes;
			$this->data['user']  = $user;
			if($_POST) {
				$rules = $this->rules();
				unset($rules[12]);
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) {
					$this->data['title'] = 'User Update';
					$this->data["subview"] = "user/edit";
					$this->load->view('_main_layout', $this->data);
				} else {
					$array = [];
					$array['name']    				= $this->input->post('name');
					$array['dob']     				= date('Y-m-d',strtotime($this->input->post('dob')));
					$array['gender']  				= $this->input->post('gender');
					$array['religion']				= $this->input->post('religion');
					$array['email']   				= $this->input->post('email');
					$array['phone']   				= $this->input->post('phone');
					$array['address'] 				= $this->input->post('address');
					$array['jod']     				= date('Y-m-d',strtotime($this->input->post('jod')));
					$array['photo']   				= $this->upload_data['photo']['file_name'];;
					$array['usertypeID']			= $this->input->post('usertypeID');
					$array['status']  				= $this->input->post('status');
					$array['username']				= $this->input->post('username');
					$array['create_date']      		= date('Y-m-d H:i:s');
					$array['create_userID']    		= $this->session->userdata('userID');
					$array['create_usertypeID']		= $this->session->userdata('usertypeID');
					$array['modify_date']      		= date('Y-m-d H:i:s');
					$array['modify_userID']    		= $this->session->userdata('userID');
					$array['modify_usertypeID']		= $this->session->userdata('usertypeID');

					$this->user_m->update_user($array, $userID);
					$this->session->set_flashdata('message','Success');
					redirect('user/index');
				}
			} else {
				$this->data['title'] = 'User Update';
				$this->data["subview"] = "user/edit";
				$this->load->view('_main_layout', $this->data);
			}
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	} else {
		$this->data["subview"] = "_not_found";
		$this->load->view('_main_layout', $this->data);
	}
}

public function view() {
	$userID = htmlentities(escapeString($this->uri->segment(3)));
	if((int)$userID) {
		$user = $this->user_m->get_single_user(array('userID' => $userID));
		if(inicompute($user)) {
			$this->data['usertypes'] = pluck($this->usertype_m->get_usertype(array('usertypeID','usertype')),'usertype','usertypeID');
			$this->data['user'] = $user;
			$this->data['title'] = 'User Details';
			$this->data["subview"] = "user/view";
			$this->load->view('_main_layout', $this->data);
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	} else {
		$this->data["subview"] = "_not_found";
		$this->load->view('_main_layout', $this->data);
	}
}

public function delete() {
	$userID = $this->input->post('id');
	if((int)$userID) {
		$user = $this->user_m->get_single_user(array('userID' => $userID));
		if(inicompute($user)) {
			if($user->photo != 'default.png') {
				$userphoto = FCPATH.'uploads/users/'.$user->photo;
				if(file_exists($userphoto)) {
					unlink($userphoto);
				}
			}
			$this->user_m->delete_user($userID);
			$response['status']  = 'success';
			$response['message'] = 'Deleted Successfully ...';
			echo json_encode($response);
		} else {
			$this->data["subview"] = "_not_found";
			$this->load->view('_main_layout', $this->data);
		}
	} else {
		$this->data["subview"] = "_not_found";
		$this->load->view('_main_layout', $this->data);
	}
}

public function photo_upload() {
	$userID = htmlentities(escapeString($this->uri->segment(3)));
	$user = array();
	if((int)$userID) {
		$user = $this->user_m->get_single_user(array('userID' => $userID));
	}

	$new_file = "default.png";
	if($_FILES["photo"]['name'] !="") {
		$file_name = $_FILES["photo"]['name'];
		$random = rand(1, 10000000000000000);
		$file_name_rename = hash('sha512', $random.$this->input->post('username') . config_item("encryption_key"));
		$explode = explode('.', $file_name);
		if(inicompute($explode) >= 2) {
			$new_file = $file_name_rename.'.'.end($explode);
			$config['upload_path'] = "./uploads/users";
			$config['allowed_types'] = "gif|jpg|png";
			$config['file_name'] = $new_file;
			$config['max_size'] = '2048';
			$config['max_width'] = '2000';
			$config['max_height'] = '2000';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload("photo")) {
				$this->form_validation->set_message("photo_upload", $this->upload->display_errors());
				return FALSE;
			} else {
				$this->upload_data['photo'] =  $this->upload->data();
				return TRUE;
			}
		} else {
			$this->form_validation->set_message("photo_upload", "Invalid file");
			return FALSE;
		}
	} else {
		if(inicompute($user)) {
			$this->upload_data['photo'] = array('file_name' => $user->photo);
			return TRUE;
		} else {
			$this->upload_data['photo'] = array('file_name' => $new_file);
			return TRUE;
		}
	}
}

public function check_unique_email($email) {
	$userID = htmlentities(escapeString($this->uri->segment(3)));
	if((int)$userID) {
		$user = $this->user_m->get_single_user(array('email' => $email, 'userID !=' => $userID));
		if(inicompute($user)) {
			$this->form_validation->set_message("check_unique_email", "The %s is already exits.");
			return FALSE;
		} else {
			return TRUE;
		}
	} else {
		$user = $this->user_m->get_single_user(array('email' => $email));
		if(inicompute($user)) {
			$this->form_validation->set_message("check_unique_email", "The %s is already exits.");
			return FALSE;
		} else {
			return TRUE;
		}
	}
}

public function check_unique_username($username) {
	$userID = htmlentities(escapeString($this->uri->segment(3)));
	if((int)$userID) {
		$user = $this->user_m->get_single_user(array('username' => $username, 'userID !=' => $userID));
		if(inicompute($user)) {
			$this->form_validation->set_message("check_unique_username", "The %s is already exits.");
			return FALSE;
		} else {
			return TRUE;
		}
	} else {
		$user = $this->user_m->get_single_user(array('username' => $username));
		if(inicompute($user)) {
			$this->form_validation->set_message("check_unique_username", "The %s is already exits.");
			return FALSE;
		} else {
			return TRUE;
		}
	}
}


protected function changepasswordrules() {
	$rules = array(
		array(
			'field'=> 'new_password',
			'label'=> $this->lang->line('user_new_password'),
			'rules'=> 'required|max_length[15]',
			),
		array(
			'field'=> 'confirm_password',
			'label'=> $this->lang->line('user_confirm_password'),
			'rules'=> 'required|matches[new_password]',
			)
		);
	return $rules;
}

public function changepassword(){
	if($_POST) {
		$this->form_validation->set_rules($this->changepasswordrules());
		if($this->form_validation->run() == FALSE) {
			$validation_errors = $this->form_validation->verror_array();
			$json = array("confirmation" => 'error', 'validations' => $validation_errors);
			header("Content-Type: application/json", true);
			echo json_encode($json);
			exit;
		} else {
			$userID = $this->input->post('id');
			$array = array(
				'password'    		=> $this->user_m->hash($this->input->post('new_password')),
				'modify_date'      	=> date('Y-m-d H:i:s'),
				'modify_userID'    	=> $this->session->userdata('userID'),
				'modify_usertypeID'	=> $this->session->userdata('usertypeID'),
				);
			$this->user_m->update_user($array, $userID);
			$json = array("confirmation" => 'Success');
			header("Content-Type: application/json", true);
			echo json_encode($json);
		}
	} else {
		redirect(base_url('user'));
	}
}
}
