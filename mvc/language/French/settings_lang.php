<?php 

$lang['settings_settings']     = "Paramètres";
$lang['settings_general_setting']     = "réglages généraux";
$lang['settings_list']     = "Lister";

$lang['settings_company_name']      = "Nom de la compagnie";
$lang['settings_tag_line']      = "Slogan";
$lang['settings_type']      = "Type d'entreprise";
$lang['settings_owner']      = "Le nom du propriétaire";
$lang['settings_mobile']      = "Numéro de portable";
$lang['settings_phone']      = "Numéro de téléphone";
$lang['settings_fax']      = "Numéro de fax";
$lang['settings_email']      = "E-mail";
$lang['settings_tax']      = "Numéro d'identification fiscale";
$lang['settings_address']      = "Adresse";
$lang['settings_time_zone']      = "Fuseau horaire";
$lang['settings_logo']      = "Logo d'entreprise";
$lang['settings_currency_code']  = "Code de devise";
$lang['settings_currency_symbol']  = "Symbole de la monnaie";
$lang['settings_prefixes']  = "Préfixes";
$lang['category_prefixe']  = "Catégorie";
$lang['member_prefixe']  = "Membre";
$lang['book_prefixe']  = "Livre";
$lang['membership_prefixe']  = "Adhésion";
$lang['settings_language']  = "Langue";

$lang['settings_update']     = "Mettre à jour";
$lang['settings_action'] 	  = "action";

?>