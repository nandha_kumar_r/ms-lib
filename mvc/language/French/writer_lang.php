<?php 

$lang['writer_writer']     = "l' auteur";
$lang['writer_add_writer']     = "Ajouter un écrivain";
$lang['writer_list']     = "Lister";

$lang['writer_name']       = "Nom";
$lang['writer_note']  	  = "Noter";
$lang['writer_status']     = "Statut";
$lang['writer_action'] 	  = "action";


$lang['writer_insert'] = "Insérer";
$lang['writer_update'] = "Mettre à jour";

?>