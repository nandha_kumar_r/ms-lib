<?php 

$lang['categories_categories']     = "Catégories";
$lang['categories_add_categories']     = "ajouter une catégorie";
$lang['categories_list']     = "liste";

$lang['categories_code']       = "Code";
$lang['categories_name']       = "Nom";
$lang['categories_note']  	  = "Noter";
$lang['categories_status']     = "Statut";
$lang['categories_action'] 	  = "action";


$lang['categories_insert'] = "Insérer";
$lang['categories_update'] = "Mettre à jour";

?>