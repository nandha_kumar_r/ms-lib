<?php 

$lang['genarateid_genarateid']     	 = "Générer une carte d'identité";
$lang['genarateid_please_select']    = "Veuillez sélectionner";
$lang['genarateid_label']     	  	 = "Sélectionnez un membre";
$lang['genarateid_generate']     	 = "produire";
$lang['genarateid_tooltip']     	 = "Sélectionnez plusieurs membres";
$lang['genarateid_details']       	 = "Des détails";
$lang['genarateid_print']       	 = "Télécharger";
$lang['genarateid_name']       	 	 = "Nom";
$lang['genarateid_joined']       	 = "a joint";
$lang['genarateid_membership']       = "Adhésion";
$lang['genarateid_gender']       	 = "Genre";


?>