<?php 

$lang['genarateid_genarateid']     	 = "إنشاء بطاقة الهوية";
$lang['genarateid_please_select']    = "الرجاء التحديد";
$lang['genarateid_label']     	  	 = "حدد عضو";
$lang['genarateid_generate']     	 = "يولد";
$lang['genarateid_tooltip']     	 = "حدد عدة أعضاء";
$lang['genarateid_details']       	 = "تفاصيل";
$lang['genarateid_print']       	 = "تحميل";
$lang['genarateid_name']       	 	 = "اسم";
$lang['genarateid_joined']       	 = "انضم";
$lang['genarateid_membership']       = "عضوية";
$lang['genarateid_gender']       	 = "جنس";


?>