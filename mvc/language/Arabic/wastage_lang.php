<?php 

$lang['wastage_wastage']     = "الهدر";
$lang['wastage_add_wastage']     = "أضف الهدر";
$lang['wastage_list']     = "قائمة";

$lang['wastage_photo']       = "صورة";
$lang['wastage_book_code']       = "كود الكتاب";
$lang['wastage_book_select']       = "حدد اسم الكتاب";
$lang['wastage_book_name']  	  = "اسم الكتاب";
$lang['wastage_writer_name']     = "اسم الكاتب";
$lang['wastage_wastager_by']     = "الهدر بواسطة";
$lang['wastage_date']     = "تاريخ";
$lang['wastage_payable_amount']     = "المبلغ المستحق";
$lang['wastage_note']     = "ملحوظة";
$lang['wastage_action'] 	  = "Action";

$lang['wastage_insert'] = "إدراج";
$lang['wastage_update'] = "تحديث";

?>