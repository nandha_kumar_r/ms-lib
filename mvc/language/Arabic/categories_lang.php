<?php 

$lang['categories_categories']     = "فئات";
$lang['categories_add_categories']     = "أضف فئات";
$lang['categories_list']     = "قائمة";

$lang['categories_code']       = "شفرة";
$lang['categories_name']       = "اسم";
$lang['categories_note']  	  = "ملحوظة";
$lang['categories_status']     = "حالة";
$lang['categories_action'] 	  = "عمل";


$lang['categories_insert'] = "إدراج";
$lang['categories_update'] = "تحديث";

?>