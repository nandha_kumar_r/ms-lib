<?php 

$lang['book_book']     			= "الكتاب";
$lang['book_add_book']     		= "أضف كتاب";
$lang['book_update']     		= "تحديث";
$lang['book_add']     			= "يضيف";
$lang['book_please_select']     = "الرجاء التحديد";
$lang['book_info']     			= "معلومات الكتاب";
$lang['book_list']     			= "قائمة";
$lang['book_name']     			= "اسم";
$lang['book_categories']     	= "فئات";
$lang['book_publication']     	= "النشر";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "كاتب";
$lang['book_edition']     		= "الإصدار";
$lang['book_edition_year']     	= "سنة الإصدار";
$lang['book_photo']     		= "صورة";
$lang['book_price']     		= "سعر";
$lang['book_quantity'] 			= "كمية";
$lang['book_availability'] 		= "التوفر";
$lang['book_rack_no']    		= "رف لا";
$lang['book_purchase_price']    = "سعر الشراء";

$lang['book_details']     		= "تفاصيل";
$lang['book_entry']     		= "دخول";


$lang['book_code']     			= "شفرة";
$lang['book_status']     		= "حالة";
$lang['book_action'] 			= "عمل";
$lang['book_code_tooltip']		= "لديك الفرصة لاستخدام الرمز الخاص بك لمكتبتك";
$lang['book_issued_quantity_tooltip']		= "عدد الكتب التي سيتم إصدارها للأعضاء";
$lang['book_issued_quantity']		= "عدد الكتب الصادرة";
$lang['isbn_tooltip']			= "الرقم العالمي الموحد للكتاب";


?>