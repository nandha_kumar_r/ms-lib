<?php 

$lang['payment_payment']     	= "ادائیگی";
$lang['payment_add_payment']    = "ادائیگی شامل کریں";
$lang['payment_list']     		= "فہرست";
$lang['payment_member_name']    = "رکن کا نام";
$lang['payment_for']     		= "ادائیگی";
$lang['payment_amount']     	= "قابل ادائیگی";
$lang['payment_date']     		= "تاریخ";
$lang['payment_status']     	= "حالت";
$lang['payment_action'] 	  	= "عمل";
$lang['payment_receipt'] 	  	= "رسید";
$lang['payment_money_receipt'] 	= "رقم کی رسید";
$lang['payment_email'] 			= "ای میل";
$lang['payment_phone'] 			= "رابطے کا نمبر.";
$lang['payment_mr'] 			= "مسٹر";
$lang['payment_from'] 	  		= "نقد وصول ہوا";
$lang['payment_of'] 	  		= "کے";
$lang['payment_for'] 	  		= "کے لئے";
$lang['payment_inword'] 	  	= "لفظ میں";
$lang['payment_received_by'] 	= "کی طرف سے موصول";

?>