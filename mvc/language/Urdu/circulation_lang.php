<?php 

$lang['circulation_circulation']     = "گردش";
$lang['circulation_add_circulation']     = "زمرہ شامل کریں";
$lang['circulation_issue_and_return']     = "جاری اور واپس";
$lang['circulation_list']     = "فہرست";

$lang['circulation_member_code'] = "ممبر کوڈ";
$lang['circulation_member_id'] = "رکن کی شناخت";
$lang['circulation_member_name'] = "رکن کا نام";
$lang['circulation_book_name'] = "کتاب کا نام";
$lang['circulation_book_code'] = "کتاب کا کوڈ";
$lang['circulation_book_code_tooltip'] = "اپنا کتابی کوڈ رکھیں";
$lang['circulation_writer_name'] = "مصنف کا نام";
$lang['circulation_issue_date'] = "تاریخ اجراء";
$lang['circulation_expiry_date'] = "واپسی کی آخری تاریخ";
$lang['circulation_return_date'] = "واپسی کی تاریخ";
$lang['circulation_return_status'] = "واپسی کی حیثیت";
$lang['circulation_penalty'] = "جرمانہ";
$lang['circulation_penalty_fee'] = "جرمانہ فیس";
$lang['circulation_no_of_days'] = "دن کی تعداد";
$lang['circulation_penalty_message'] = "اس کتاب کی واپسی کی تاریخ گزر چکی ہے۔ براہ کرم جرمانہ ادا کریں۔";
$lang['circulation_penalty_note'] = "نوٹ";
$lang['member_penalty_amount'] = "کل رقم";
$lang['circulation_action'] 	  = "عمل";

$lang['circulation_insert'] = "داخل کریں";
$lang['circulation_update'] = "اپ ڈیٹ";
$lang['circulation_search'] = "تلاش کریں";
$lang['circulation_details'] = "تفصیلات";
$lang['circulation_issue'] = "مسئلہ کتاب";
$lang['circulation_issued'] = "مسئلہ";
$lang['circulation_not_available'] = "دستیاب نہیں ہے";
$lang['circulation_member_search'] = "ممبر تلاش کریں";

$lang['circulation_member_gender'] = "صنف";
$lang['circulation_member_occupation'] = "قبضہ";
$lang['circulation_member_phone'] = "فون";
$lang['circulation_book_return'] = "واپس";
$lang['circulation_book_renewal'] = "تجدید";
$lang['circulation_book_lost'] = "کھو دیا";
$lang['circulation_book_lost_message'] = "چونکہ آپ کی کتاب کھو گئی ہے ، آپ کو کتاب کی قیمت ادا کرنا ہوگی۔";
$lang['circulation_book_price'] = "کتاب کی قیمت";
$lang['circulation_book_payable_amount'] = "قابل ادائیگی";

$lang['book_photo'] = "تصویر";
$lang['book_code'] = "کوڈ";
$lang['book_name'] = "نام";
$lang['book_writer'] = "لکھاری";
$lang['book_edition'] = "ایڈیشن";
$lang['book_price'] = "قیمت";
$lang['book_availability'] = "دستیابی";
$lang['book_rack_no'] = "ریک نمبر";
$lang['book_action'] = "عمل";

?>