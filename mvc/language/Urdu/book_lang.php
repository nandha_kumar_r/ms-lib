<?php 

$lang['book_book']     			= "کتاب";
$lang['book_add_book']     		= "کتاب شامل کریں";
$lang['book_update']     		= "اپ ڈیٹ";
$lang['book_add']     			= "شامل کریں";
$lang['book_please_select']     = "براہ مہربانی منتخب کریں";
$lang['book_info']     			= "کتاب کی معلومات";
$lang['book_list']     			= "فہرست";
$lang['book_name']     			= "نام";
$lang['book_categories']     	= "اقسام";
$lang['book_publication']     	= "اشاعت";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "لکھاری";
$lang['book_edition']     		= "ایڈیشن";
$lang['book_edition_year']     	= "ایڈیشن کا سال";
$lang['book_photo']     		= "تصویر";
$lang['book_price']     		= "قیمت";
$lang['book_quantity'] 			= "مقدار";
$lang['book_availability'] 		= "دستیابی";
$lang['book_rack_no']    		= "ریک نمبر";
$lang['book_purchase_price']    = "قیمت خرید";

$lang['book_details']     		= "تفصیلات";
$lang['book_entry']     		= "اندراج";

$lang['book_code']     			= "کوڈ";
$lang['book_status']     		= "حالت";
$lang['book_action'] 			= "عمل";
$lang['book_code_tooltip']		= "آپ کو اپنی لائبریری کے لئے اپنا کوڈ استعمال کرنے کا موقع ملا ہے";
$lang['book_issued_quantity_tooltip']		= "ممبروں کے لئے جاری کی جانے والی کتب کی تعداد";
$lang['book_issued_quantity']		= "جاری کردہ کتب کی تعداد";
$lang['isbn_tooltip']			= "بین الاقوامی معیاری کتاب نمبر";

?>