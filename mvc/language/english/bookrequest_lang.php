<?php 

$lang['bookrequest_bookrequest']     	= "Book Request";
$lang['bookrequest_add_bookrequest']    = "Requested Book";
$lang['bookrequest_list']     			= "List";
$lang['bookrequest_name']       		= "Book Name";
$lang['bookrequest_writer_name']       	= "Writer Name";
$lang['bookrequest_categories']       	= "Categories";
$lang['bookrequest_edition']       		= "Edition";
$lang['bookrequest_note']  	  			= "Note";
$lang['bookrequest_member']  	  		= "Member Name";
$lang['bookrequest_action'] 	  		= "Action";
$lang['bookrequest_insert'] 			= "Insert";
$lang['bookrequest_update'] 			= "Update";

?>