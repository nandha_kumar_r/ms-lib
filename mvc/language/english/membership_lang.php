<?php 

$lang['membership_membership']     = "Membership";
$lang['membership_add_membership']     = "Add Membership";
$lang['membership_list']     = "List";

$lang['membership_code']      = "Code";
$lang['membership_name']      = "Name";
$lang['membership_limit_book']  = "Book Limit";
$lang['membership_limit_day']  = "Day limit";
$lang['membership_fee']  = "Membership Fee";
$lang['membership_penalty_fee']  = "Penalty Fee";
$lang['membership_renew_limit']  = "Renewal Limit";
$lang['membership_free']  = "Free";
$lang['membership_status']     = "Status";
$lang['membership_action'] 	  = "Action";

$lang['membership_add_to_cart'] = "Add To Cart";
$lang['membership_insert'] = "Insert";
$lang['membership_error'] = "Something is wrong";
$lang['membership_update'] = "Update";

$lang['membership_note'] = "Note:";
$lang['membership_penalty_note'] = "1. Penalty fee will be added according to the day.";
$lang['membership_renew_note'] = "2. The renewal limit applies to the book.";

?>