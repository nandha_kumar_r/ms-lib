<?php 

$lang['book_book']     			= "Book";
$lang['book_add_book']     		= "Add Book";
$lang['book_update']     		= "Update";
$lang['book_add']     			= "Add";
$lang['book_please_select']     = "Please Select";
$lang['book_info']     			= "Book Info";
$lang['book_list']     			= "List";
$lang['book_name']     			= "Name";
$lang['book_categories']     	= "Categories";
$lang['book_publication']     	= "Publication";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "Writer";
$lang['book_edition']     		= "Edition";
$lang['book_edition_year']     	= "Edition Year";
$lang['book_photo']     		= "Photo";
$lang['book_price']     		= "Price";
$lang['book_quantity'] 			= "Quantity";
$lang['book_availability'] 		= "Availability";
$lang['book_rack_no']    		= "Rack no.";
$lang['book_purchase_price']    = "Purchase Price";

$lang['book_details']     		= "Details";
$lang['book_entry']     		= "Entry";


$lang['book_code']     			= "Code";
$lang['book_status']     		= "Status";
$lang['book_action'] 			= "Action";
$lang['book_code_tooltip']		= "You have the opportunity to use your own code for your library";
$lang['book_issued_quantity_tooltip']		= "Number of Books to be Issued for Members";
$lang['book_issued_quantity']		= "Issued Quantity";
$lang['isbn_tooltip']			= "International Standard Book Number";


?>