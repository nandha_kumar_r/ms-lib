<?php 

$lang['circulation_circulation']     = "Circulation";
$lang['circulation_add_circulation']     = "Add Category";
$lang['circulation_issue_and_return']     = "Issued & Returned";
$lang['circulation_list']     = "List";

$lang['circulation_member_code'] = "Member Code";
$lang['circulation_member_id'] = "Member ID";
$lang['circulation_member_name'] = "Member Name";
$lang['circulation_book_name'] = "Book Name";
$lang['circulation_book_code'] = "Book Code";
$lang['circulation_book_code_tooltip'] = "Put your Book Code";
$lang['circulation_writer_name'] = "Writer Name";
$lang['circulation_issue_date'] = "Issue Date";
$lang['circulation_expiry_date'] = "Last Date to return";
$lang['circulation_return_date'] = "Return Date";
$lang['circulation_return_status'] = "Return Status";
$lang['circulation_penalty'] = "Penalty";
$lang['circulation_penalty_fee'] = "Penalty Fee";
$lang['circulation_no_of_days'] = "No. of Days";
$lang['circulation_penalty_message'] = "Book reutrn date is over. Please pay the penalty.";
$lang['circulation_penalty_note'] = "Note";
$lang['member_penalty_amount'] = "Total Amount";
$lang['circulation_action'] 	  = "Action";


$lang['circulation_insert'] = "Insert";
$lang['circulation_update'] = "Update";
$lang['circulation_search'] = "Search";
$lang['circulation_details'] = "Details";
$lang['circulation_issue'] = "Issue Book";
$lang['circulation_issued'] = "Issue";
$lang['circulation_not_available'] = "Not Available";
$lang['circulation_member_search'] = "Search Member";

$lang['circulation_member_gender'] = "Gender";
$lang['circulation_member_occupation'] = "Occupation";
$lang['circulation_member_phone'] = "Phone";
$lang['circulation_book_return'] = "Return";
$lang['circulation_book_renewal'] = "Renewal";
$lang['circulation_book_lost'] = "Lost";
$lang['circulation_book_lost_message'] = "Since you lost the book, you have to pay price of the book.";
$lang['circulation_book_price'] = "Book Price";
$lang['circulation_book_payable_amount'] = "Payable Amount";


$lang['book_photo'] = "Photo";
$lang['book_code'] = "Code";
$lang['book_name'] = "Name";
$lang['book_writer'] = "Writer";
$lang['book_edition'] = "Edition";
$lang['book_price'] = "Price";
$lang['book_availability'] = "Availability";
$lang['book_rack_no'] = "Rack no.";
$lang['book_action'] = "Action";

?>