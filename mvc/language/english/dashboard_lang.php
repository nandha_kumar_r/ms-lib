<?php 

$lang['dashboard_dashboard']     = "Dashboard";
$lang['dashboard_more']  = "More Info";
$lang['dashboard_member']     = "Members";
$lang['dashboard_book'] 	  = "Books";
$lang['dashboard_categories'] 	  = "Categories";
$lang['dashboard_membership'] = "Membership";
$lang['dashboard_issued'] = "Issued";
$lang['dashboard_wastage'] = "Wastage";
$lang['dashboard_publication'] = "Publication";
$lang['dashboard_writer'] = "Writer";
$lang['dashboard_bar_chat'] = "Issued & Returned Bar Chart";
$lang['dashboard_issued_book'] = "Last 5 Issued Book";
$lang['dashboard_returned_book'] = "Last 5 Returned Book";
$lang['circulation_member_name'] = "Member Name";
$lang['circulation_book_name'] = "Book Name";
$lang['circulation_writer_name'] = "Writer Name";
$lang['circulation_issue_date'] = "Issue Date";
$lang['circulation_expiry_date'] = "Last Date to return";
$lang['circulation_return_date'] = "Return Date";
$lang['categories_code'] = "Code";
$lang['categories_name'] = "Name";

?>