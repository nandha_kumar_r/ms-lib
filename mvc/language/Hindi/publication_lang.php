<?php 

$lang['publication_publication']     = "प्रकाशन";
$lang['publication_add_publication']     = "प्रकाशन जोड़ें";
$lang['publication_list']     = "सूची";

$lang['publication_name']       = "नाम";
$lang['publication_note']  	  = "टिप्पणी";
$lang['publication_status']     = "स्थिति";
$lang['publication_action'] 	  = "Action";

$lang['publication_insert'] = "डालने";
$lang['publication_update'] = "अपडेट करें";

?>