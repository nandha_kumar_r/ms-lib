<?php 

$lang['user_user']     = "उपयोगकर्ता";
$lang['user_add_user']     = "उपयोगकर्ता जोड़ें";
$lang['user_add']     = "जोड़ना";
$lang['user_list']     = "सूची";
$lang['user_name']     = "नाम";
$lang['user_dob']      = "जन्म की तारीख";
$lang['user_gender']   = "लिंग";
$lang['user_please_select']   = "कृपया चयन कीजिए";
$lang['user_male']   = "पुरुष";
$lang['user_female']   = "महिला";
$lang['user_religion'] = "धर्म";
$lang['user_email']    = "ईमेल";
$lang['user_phone']    = "फ़ोन";
$lang['user_address']  = "पता";
$lang['user_jod']      = "शामिल होने की तिथि";
$lang['user_photo']    = "तस्वीर";
$lang['user_active']   = "सक्रिय";
$lang['user_usertypeID'] = "उपयोगकर्ता प्रकार आईडी";
$lang['user_username'] = "उपयोगकर्ता नाम";
$lang['user_password'] = "कुंजिका";
$lang['user_role'] 	= "भूमिका";
$lang['user_status'] = "स्थिति";
$lang['user_active'] = "सक्रिय";
$lang['user_deactive'] = "निष्क्रिय करें";
$lang['user_action'] = "कार्य";
$lang['user_update'] = "अपडेट करें";
$lang['user_profile'] = "प्रोफ़ाइल";
$lang['user_change_password'] = "पासवर्ड बदलें";
$lang['user_new_password'] = "नया पासवर्ड";
$lang['user_confirm_password'] = "पासवर्ड की पुष्टि कीजिये";

?>