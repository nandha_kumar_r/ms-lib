<?php 

$lang['settings_settings']     = "समायोजन";
$lang['settings_general_setting']     = "सामान्य सेटिंग्स";
$lang['settings_list']     = "सूची";

$lang['settings_company_name']      = "कंपनी का नाम";
$lang['settings_tag_line']      = "टैग लाइन";
$lang['settings_type']      = "व्यापार के प्रकार";
$lang['settings_owner']      = "मालिक का नाम";
$lang['settings_mobile']      = "मोबाइल नंबर";
$lang['settings_phone']      = "फोन नंबर।";
$lang['settings_fax']      = "फैक्स नंबर";
$lang['settings_email']      = "ईमेल";
$lang['settings_tax']      = "कर संख्या";
$lang['settings_address']      = "पता";
$lang['settings_time_zone']      = "समय क्षेत्र";
$lang['settings_logo']      = "कंपनी का लोगो";
$lang['settings_currency_code']  = "मुद्रा कोड";
$lang['settings_currency_symbol']  = "मुद्रा चिन्ह";
$lang['settings_prefixes']  = "उपसर्गों";
$lang['category_prefixe']  = "वर्ग";
$lang['member_prefixe']  = "सदस्य";
$lang['book_prefixe']  = "पुस्तक";
$lang['membership_prefixe']  = "सदस्यता";
$lang['settings_language']  = "भाषा";

$lang['settings_update']     = "अपडेट करें";
$lang['settings_action'] 	  = "कार्य";

?>