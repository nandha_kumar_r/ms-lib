<?php 

$lang['wastage_wastage']     = "क्षय";
$lang['wastage_add_wastage']     = "अपव्यय जोड़ें";
$lang['wastage_list']     = "सूची";

$lang['wastage_photo']       = "तस्वीर";
$lang['wastage_book_code']       = "पुस्तक कोड";
$lang['wastage_book_select']       = "पुस्तक का नाम चुनें";
$lang['wastage_book_name']  	  = "पुस्तक का नाम";
$lang['wastage_writer_name']     = "लेखक का नाम";
$lang['wastage_wastager_by']     = "अपव्यय बाय";
$lang['wastage_date']     = "तारीख";
$lang['wastage_payable_amount']     = "भुगतान योग्य राशि";
$lang['wastage_note']     = "टिप्पणी";
$lang['wastage_action'] 	  = "कार्य";

$lang['wastage_insert'] = "डालने";
$lang['wastage_update'] = "अपडेट करें";

?>