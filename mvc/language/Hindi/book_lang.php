<?php 

$lang['book_book']     			= "किताब";
$lang['book_add_book']     		= "किताब जोड़ें";
$lang['book_update']     		= "अपडेट करें";
$lang['book_add']     			= "जोड़ना";
$lang['book_please_select']     = "कृपया चयन कीजिए";
$lang['book_info']     			= "पुस्तक जानकारी";
$lang['book_list']     			= "सूची";
$lang['book_name']     			= "नाम";
$lang['book_categories']     	= "प्रकाशन";
$lang['book_publication']     	= "प्रकाशन";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "लेखक";
$lang['book_edition']     		= "संस्करण";
$lang['book_edition_year']     	= "संस्करण वर्ष";
$lang['book_photo']     		= "तस्वीर";
$lang['book_price']     		= "मूल्य";
$lang['book_quantity'] 			= "मात्रा";
$lang['book_availability'] 		= "उपलब्धता";
$lang['book_rack_no']    		= "रैक नंबर";
$lang['book_purchase_price']    = "खरीद मूल्य";

$lang['book_details']     		= "विवरण";
$lang['book_entry']     		= "पुस्तक प्रविष्टि";


$lang['book_code']     			= "कोड";
$lang['book_status']     		= "स्थिति";
$lang['book_action'] 			= "कार्य";
$lang['book_code_tooltip']		= "आपके पास अपनी लाइब्रेरी के लिए अपने कोड का उपयोग करने का अवसर है";
$lang['book_issued_quantity_tooltip']		= "सदस्यों के लिए जारी की जाने वाली पुस्तकों की संख्या";
$lang['book_issued_quantity']		= "जारी की गई पुस्तकों की संख्या";
$lang['isbn_tooltip']			= "International Standard Book Number";


?>