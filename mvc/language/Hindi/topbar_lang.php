<?php 
$lang['add']      = "जोड़ना";
$lang['edit']     = "संपादित करें";
$lang['view']     = "देखना";
$lang['delete']   = "हटाएं";

$lang['menu_dashboard']     = "डैशबोर्ड";
$lang['menu_user']          = "उपयोगकर्ताओं";
$lang['menu_newuser']          = "उपयोगकर्ता";
$lang['menu_usertype']      = "उपयोगकर्ता भूमिका";

$lang['menu_permissions']   = "अनुमतियां";
$lang['menu_permissionlog'] = "अनुमति लॉग";
$lang['menu_permissionmodule'] = "अनुमति मॉड्यूल";
$lang['menu_menu']             = "मेन्यू";

$lang['menu_settings']  = "समायोजन";
$lang['menu_sitesettings']  = "कंपनी प्रोफाइल";
$lang['menu_membership']  = "सदस्यता";
$lang['menu_member']  = "सदस्य";
$lang['menu_memberlist']  = "सदस्य सूची";
$lang['menu_categories']  = "श्रेणियाँ";
$lang['menu_book']  = "पुस्तक";
$lang['menu_book_archive']  = "पुरालेख";
$lang['menu_wastage']  = "क्षय";
$lang['menu_publication']  = "प्रकाशन";
$lang['menu_circulation']  = "प्रसार";
$lang['menu_payment']  = "भुगतान";
$lang['menu_report']  = "रिपोर्ट";
$lang['menu_writer']  = "लेखक";
$lang['menu_emailsetting']  = "ईमेल कॉन्फिग";
$lang['menu_bookrequest']  = "पुस्तक अनुरोध";
$lang['menu_genarateid']  = "आईडी कार्ड जनरेट करें";
$lang['menu_import']  = "सीएसवी आयात करें";
?>