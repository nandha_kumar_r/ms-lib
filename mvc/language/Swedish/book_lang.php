<?php 
$lang['book_book']     			= "bok";
$lang['book_add_book']     		= "Lägg till bok";
$lang['book_update']     		= "Uppdatering";
$lang['book_add']     			= "Lägg till";
$lang['book_please_select']     = "Vänligen välj";
$lang['book_info']     			= "Bokinfo";
$lang['book_list']     			= "Lista";
$lang['book_name']     			= "namn";
$lang['book_categories']     	= "Kategorier";
$lang['book_publication']     	= "Offentliggörande";
$lang['book_isbn']     			= "ISBN";
$lang['book_writer']     		= "Författare";
$lang['book_edition']     		= "Utgåva";
$lang['book_edition_year']     	= "Utgåva år";
$lang['book_photo']     		= "Foto";
$lang['book_price']     		= "Pris";
$lang['book_quantity'] 			= "Kvantitet";
$lang['book_availability'] 		= "Tillgänglighet";
$lang['book_rack_no']    		= "Rack nr.";
$lang['book_purchase_price']    = "Köp pris";

$lang['book_details']     		= "Detaljer";
$lang['book_entry']     		= "Inträde";

$lang['book_code']     			= "Koda";
$lang['book_status']     		= "Status";
$lang['book_action'] 			= "Handling";
$lang['book_code_tooltip']		= "Du har möjlighet att använda din egen kod för ditt bibliotek";
$lang['book_issued_quantity_tooltip']		= "Antal böcker som ska utfärdas för medlemmar";
$lang['book_issued_quantity']		= "Utfärdat antal";
$lang['isbn_tooltip']			= "Internationellt standardboknummer";

?>