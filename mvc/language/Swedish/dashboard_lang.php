<?php 

$lang['dashboard_dashboard']     = "instrumentbräda";
$lang['dashboard_more']  = "Mer information";
$lang['dashboard_member']     = "Medlemmar";
$lang['dashboard_book'] 	  = "Böcker";
$lang['dashboard_categories'] 	  = "Kategorier";
$lang['dashboard_membership'] = "Medlemskap";
$lang['dashboard_issued'] = "Utfärdad";
$lang['dashboard_wastage'] = "Slöseri";
$lang['dashboard_publication'] = "Offentliggörande";
$lang['dashboard_writer'] = "Författare";
$lang['dashboard_bar_chat'] = "Utfärdad och returnerad stapeldiagram";
$lang['dashboard_issued_book'] = "Senaste 5 utgivna boken";
$lang['dashboard_returned_book'] = "Sista 5 returnerade boken";
$lang['circulation_member_name'] = "Medlemsnamn";
$lang['circulation_book_name'] = "Boknamn";
$lang['circulation_writer_name'] = "Författarens namn";
$lang['circulation_issue_date'] = "Utfärdsdatum";
$lang['circulation_expiry_date'] = "Senaste datum att återvända";
$lang['circulation_return_date'] = "Återlämningsdatum";
$lang['categories_code'] = "Koda";
$lang['categories_name'] = "namn";

?>