<?php 

$lang['genarateid_genarateid']     	 = "জেনারেট আইডি কার্ড";
$lang['genarateid_please_select']    = "নির্বাচন করুন";
$lang['genarateid_label']     	  	 = "সদস্য নির্বাচন";
$lang['genarateid_generate']     	 = "জেনারেট";
$lang['genarateid_tooltip']     	 = "আপনি একাধিক সদস্য নির্বাচন করতে পারেন";
$lang['genarateid_details']       	 = "বিশদ বিবরণ";
$lang['genarateid_print']       	 = "ডাউনলোড";
$lang['genarateid_name']       	 	 = "নাম";
$lang['genarateid_joined']       	 = "যোগদান করেছেন";
$lang['genarateid_membership']       = "সদস্যপদ";
$lang['genarateid_gender']       	 = "লিঙ্গ";


?>