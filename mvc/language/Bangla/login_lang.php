<?php 

$lang['login_username']    = "ইউজারনেম";
$lang['login_password']    = "পাসওয়ার্ড";
$lang['login_sign_in']     = "সাইন ইন";
$lang['retry']     = "লগইন পুনরায় চেষ্টা করুন";
$lang['login_remember_me'] = "লগইন তথ্য সংরক্ষণ করুন";
$lang['forgot_password'] = "আমি আমার পাসওয়ার্ড ভুলে গেছি";
$lang['forgot_password_header'] = "পাসওয়ার্ড ভুলে গেছেন";
$lang['forgot_password_sms'] = "আপনার পাসওয়ার্ড এখানে রিসেট করুন";
$lang['forgot_email_address'] = "ইমেল ঠিকানা";
$lang['reset_password'] = "পাসওয়ার্ড রিসেট করুন";
$lang['reset_password_sms'] = "আপনার নতুন পাসওয়ার্ড সেট করুন";
$lang['new_password'] = "নতুন পাসওয়ার্ড";
$lang['confirm_new_password'] = "নিশ্চিত করুন নতুন পাসওয়ার্ড ";
$lang['validation'] = "ভেরিফিকেশন প্রক্রিয়া";
$lang['validation_code'] = "ভেরিফিকেশন কোড";
$lang['validation_sms'] = "দয়া করে ই-মেইল থেকে বৈধতা কোডটি কপি করুন এবং এখানে পেস্ট করুন";
$lang['verify_button'] = "যাচাই করুন";
$lang['check_validation_code'] = "বৈধতা কোড অবৈধ";

?>