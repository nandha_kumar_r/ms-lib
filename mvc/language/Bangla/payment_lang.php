<?php 

$lang['payment_payment']     	= "পেমেন্ট";
$lang['payment_add_payment']    = "পেমেন্ট সংযোজন";
$lang['payment_list']     		= "তালিকা";
$lang['payment_member_name']    = "সদস্যের নাম";
$lang['payment_for']     		= "পেমেন্টর কারন";
$lang['payment_amount']     	= "পেমেন্টর পরিমান";
$lang['payment_date']     		= "তারিখ";
$lang['payment_status']     	= "অবস্থা";
$lang['payment_action'] 	  	= "ক্রিয়া";
$lang['payment_receipt'] 	  	= "রসিদ";
$lang['payment_money_receipt'] 	= "মানি রিসিট";
$lang['payment_email'] 			= "ইমেল";
$lang['payment_phone'] 			= "যোগাযোগের নম্বর";
$lang['payment_mr'] 			= "জনাব";
$lang['payment_from'] 	  		= "থেকে নগদ প্রাপ্তি";
$lang['payment_of'] 	  		= "এত";
$lang['payment_for'] 	  		= "এইজন্য";
$lang['payment_inword'] 	  	= "কথায়";
$lang['payment_received_by'] 	= "গ্রহণকারী";


?>