<?php 
$lang['add']      = "Agregar";
$lang['edit']     = "Editar";
$lang['view']     = "Vista";
$lang['delete']   = "Borrar";

$lang['menu_dashboard']     = "Tablero";
$lang['menu_user']          = "Usuarios";
$lang['menu_newuser']          = "Usuario";
$lang['menu_usertype']      = "Rol del usuario";

$lang['menu_permissions']   = "Permisos";
$lang['menu_permissionlog'] = "Registro de permisos";
$lang['menu_permissionmodule'] = "Módulo de permisos";
$lang['menu_menu']             = "Menú";

$lang['menu_settings']  = "Ajustes";
$lang['menu_sitesettings']  = "Perfil de la empresa";
$lang['menu_membership']  = "Afiliación";
$lang['menu_member']  = "Miembro";
$lang['menu_memberlist']  = "Lista de miembros";
$lang['menu_categories']  = "Categorías";
$lang['menu_book']  = "Libro";
$lang['menu_book_archive']  = "Archivo";
$lang['menu_wastage']  = "Pérdida";
$lang['menu_publication']  = "Publicación";
$lang['menu_circulation']  = "Circulación";
$lang['menu_payment']  = "Pago";
$lang['menu_report']  = "Informe";
$lang['menu_writer']  = "el autor";
$lang['menu_emailsetting']  = "Configuración de correo electrónico";
$lang['menu_bookrequest']  = "Solicitud de reserva";
$lang['menu_genarateid']  = "Tarjeta de identificación de Genarate";
$lang['menu_import']  = "Importar CSV";
?>