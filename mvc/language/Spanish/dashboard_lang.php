<?php 

$lang['dashboard_dashboard']     = "Tablero";
$lang['dashboard_more']  = "Más información";
$lang['dashboard_member']     = "Miembros";
$lang['dashboard_book'] 	  = "Libros";
$lang['dashboard_categories'] 	  = "Categorías";
$lang['dashboard_membership'] = "Afiliación";
$lang['dashboard_issued'] = "Emitido";
$lang['dashboard_wastage'] = "Pérdida";
$lang['dashboard_publication'] = "Publicación";
$lang['dashboard_writer'] = "el autor";
$lang['dashboard_bar_chat'] = "Gráfico de barras emitido y devuelto";
$lang['dashboard_issued_book'] = "Últimos 5 libros emitidos";
$lang['dashboard_returned_book'] = "Últimos 5 libros devueltos";
$lang['circulation_member_name'] = "Nombre de miembro";
$lang['circulation_book_name'] = "Nombre del libro";
$lang['circulation_writer_name'] = "Nombre del escritor";
$lang['circulation_issue_date'] = "Fecha de asunto";
$lang['circulation_expiry_date'] = "Última fecha de regreso";
$lang['circulation_return_date'] = "Fecha de regreso";
$lang['categories_code'] = "Código";
$lang['categories_name'] = "Nombre";

?>