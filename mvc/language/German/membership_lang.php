<?php 

$lang['membership_membership']     = "Mitgliedschaft";
$lang['membership_add_membership']     = "Mitgliedschaft hinzufügen";
$lang['membership_list']     = "Aufführen";

$lang['membership_code']      = "Code";
$lang['membership_name']      = "Name";
$lang['membership_limit_book']  = "Limit der Bücher";
$lang['membership_limit_day']  = "Begrenzung der Tage";
$lang['membership_fee']  = "Mitgliedsbeitrag";
$lang['membership_penalty_fee']  = "Bußgeld";
$lang['membership_renew_limit']  = "Verlängerungslimit";
$lang['membership_free']  = "Kostenlos";
$lang['membership_status']     = "Status";
$lang['membership_action'] 	  = "Aktion";

$lang['membership_add_to_cart'] = "In den Warenkorb legen";
$lang['membership_insert'] = "Einfügen";
$lang['membership_error'] = "Irgendwas stimmt nicht";
$lang['membership_update'] = "Aktualisieren";

$lang['membership_note'] = "Hinweis:";
$lang['membership_penalty_note'] = "1. Die Strafgebühr wird je nach Tag hinzugefügt.";
$lang['membership_renew_note'] = "2. Das Verlängerungslimit gilt für das Buch.";

?>