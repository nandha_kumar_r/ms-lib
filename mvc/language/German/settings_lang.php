<?php 

$lang['settings_settings']     = "die Einstellungen";
$lang['settings_general_setting']     = "Allgemeine Einstellungen";
$lang['settings_list']     = "Aufführen";

$lang['settings_company_name']      = "Name der Firma";
$lang['settings_tag_line']      = "Tag-Linie";
$lang['settings_type']      = "Unternehmensart";
$lang['settings_owner']      = "Besitzername";
$lang['settings_mobile']      = "Handynummer";
$lang['settings_phone']      = "Telefonnummer";
$lang['settings_fax']      = "Faxnummer";
$lang['settings_email']      = "Email";
$lang['settings_tax']      = "Steuernummer";
$lang['settings_address']      = "Adresse";
$lang['settings_time_zone']      = "Zeitzone";
$lang['settings_logo']      = "Firmenlogo";
$lang['settings_currency_code']  = "Währungscode";
$lang['settings_currency_symbol']  = "Währungszeichen";
$lang['settings_prefixes']  = "Präfixe";
$lang['category_prefixe']  = "Kategorie";
$lang['member_prefixe']  = "Mitglied";
$lang['book_prefixe']  = "Buch";
$lang['membership_prefixe']  = "Mitgliedschaft";
$lang['settings_language']  = "Sprache";

$lang['settings_update']     = "Aktualisieren";
$lang['settings_action'] 	  = "Aktion";

?>