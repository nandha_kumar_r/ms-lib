<?php 

$lang['wastage_wastage']     = "Verschwendung";
$lang['wastage_add_wastage']     = "Wstage hinzufügen";
$lang['wastage_list']     = "Aufführen";

$lang['wastage_photo']       = "Foto";
$lang['wastage_book_code']       = "Buchcode";
$lang['wastage_book_select']       = "Buchnamen auswählen";
$lang['wastage_book_name']  	  = "Buchname";
$lang['wastage_writer_name']     = "Autorname";
$lang['wastage_wastager_by']     = "Verschwendung von";
$lang['wastage_date']     = "Datum";
$lang['wastage_payable_amount']     = "Zu zahlender Betrag";
$lang['wastage_note']     = "Hinweis";
$lang['wastage_action'] 	  = "Aktion";

$lang['wastage_insert'] = "Einfügen";
$lang['wastage_update'] = "Aktualisieren";

?>