<?php 
$lang['add']      = "Hinzufügen";
$lang['edit']     = "Bearbeiten";
$lang['view']     = "Aussicht";
$lang['delete']   = "Löschen";

$lang['menu_dashboard']     = "Instrumententafel";
$lang['menu_user']          = "Benutzer";
$lang['menu_newuser']          = "Benutzer";
$lang['menu_usertype']      = "Benutzer-Rolle";

$lang['menu_permissions']   = "Berechtigungen";
$lang['menu_permissionlog'] = "Berechtigungsprotokoll";
$lang['menu_permissionmodule'] = "Berechtigungsmodul";
$lang['menu_menu']             = "Speisekarte";

$lang['menu_settings']  = "die Einstellungen";
$lang['menu_sitesettings']  = "Firmenprofil";
$lang['menu_membership']  = "Mitgliedschaft";
$lang['menu_member']  = "Mitglied";
$lang['menu_memberlist']  = "Mitgliederliste";
$lang['menu_categories']  = "Kategorien";
$lang['menu_book']  = "Buch";
$lang['menu_book_archive']  = "Archiv";
$lang['menu_wastage']  = "Verschwendung";
$lang['menu_publication']  = "Veröffentlichung";
$lang['menu_circulation']  = "Verkehr";
$lang['menu_payment']  = "Zahlung";
$lang['menu_report']  = "Bericht";
$lang['menu_writer']  = "der Autor";
$lang['menu_emailsetting']  = "E-Mail-Konfiguration";
$lang['menu_bookrequest']  = "Anfrage buchen";
$lang['menu_genarateid']  = "Ausweis erstellen";
$lang['menu_import']  = "CSV-Datei importieren";
?>