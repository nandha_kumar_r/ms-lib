 <style type="text/css">
table.dataTable tbody td {vertical-align:middle;}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('user_user')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/user')?>"><?=$this->lang->line('user_user')?></a></li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('user_list')?></h5>
        <div class="box-tools pull-right">
          <?php if(permissionChecker('user_add')){?>
            <a href="<?=base_url('user/add')?>" class="btn btn-inline btn-custom btn-md"><i class="fa fa-plus"></i> <?=$this->lang->line('user_add_user')?></a>
          <?php } ?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('user_photo')?></th>
                <th><?=$this->lang->line('user_name')?></th>
                <th><?=$this->lang->line('user_email')?></th>
                <th><?=$this->lang->line('user_role')?></th>
                <th><?=$this->lang->line('user_phone')?></th>
                <th><?=$this->lang->line('user_action')?></th>
              </tr>
            </thead>
            <tbody >
              <?php if(count($users)) { $i=0; foreach($users as $user) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('user_photo')?>"><img src="<?=profile_img($user->photo)?>" class="profile_img" alt=""></td>
                <td data-title="<?=$this->lang->line('user_name')?>"><?=$user->name?></td>
                <td data-title="<?=$this->lang->line('user_email')?>"><?=$user->email?></td>
                <td data-title="<?=$this->lang->line('user_role')?>"><?=isset($usertypes[$user->usertypeID]) ? $usertypes[$user->usertypeID] : '&nbsp;'?></td>
                <td data-title="<?=$this->lang->line('user_phone')?>"><?=(!empty($user->phone)?$user->phone:'&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('user_action')?>">
                  <?=(permissionChecker('user_view')? btn_view_show('user/view/'.$user->userID,$this->lang->line('view')):'');?>
                  <?=(permissionChecker('user_edit')? btn_edit_show('user/edit/'.$user->userID,$this->lang->line('edit')):'');?>
                  <?=changepassword_btn('user_changepassword',$user->userID); ?>
                  <?php if($this->session->userdata('userID') != $user->userID){echo delete_btn('user_delete', $user->userID);}?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="change" tabindex="-1">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-key"></i></span>&nbsp;<?=$this->lang->line('user_change_password')?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group error-name">
          <label><?=$this->lang->line('user_new_password')?> <span class='text-danger'>*</span></label>
          <input type="password" class="form-control" name="new_password" id="new_password" data-toggle="password" placeholder="<?=$this->lang->line('user_new_password')?>"/>
          <span class="text-red" id="error_new_password"></span>
        </div>
        <div class="form-group error-percentage">
          <label><?=$this->lang->line('user_confirm_password')?> <span class='text-danger'>*</span></label>
          <input type="password" name='confirm_password' id="confirm_password" class="form-control" data-toggle="password" placeholder="<?=$this->lang->line('user_confirm_password')?>"/>
          <span class="text-red" id="error_confirm_password"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default changepassword">Save</button>
      </div>
    </div>
  </div>
</div>