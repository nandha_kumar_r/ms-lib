
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Play:400,700&subset=cyrillic,cyrillic-ext,greek,latin-ext" rel="stylesheet">

  <title>qBill | Morning Sun IT</title>

  <style type="text/css">
  .panel,.alert,.btn{border-radius:0px !important;}
  .text-red{color: red;}
  .form-group.has-error label {
  color: #dd4b39;
}
.form-group.has-error .form-control,
.form-group.has-error .input-group-addon {
  border-color: #dd4b39;
  box-shadow: none;
}
.form-group.has-error .help-block {
  color: #dd4b39;
}
.done p {margin: 0px;}
@media screen and (max-width: 1199px) and (min-width: 992px){ 
  .col-md-4{
    width: 33% !important;
    margin-right: 1px!important;
  }
}
 @media(max-width: 991px){ 
  .col-md-4{
    width: none !important;
    margin-right: 0px  !important;
  }
}
 @media (min-width: 1200px){ 
  .col-md-4{
    width: 33%  !important;
    margin-right: 2px  !important;
  }
}

  .custom-form-control {
    display: block;
    width: 100%;
    height:none !important;
    padding: 6px 12px;
    font-size: 24px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: none!important;
  }

  .custom-input-field{
    display: block;
    width: 100%;
    height:none !important;
    padding: 6px 12px;
    font-size: 16px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: none!important;
  }

  .col-md-3{padding-left: 15px !important;}

  .stepwizard-step p {
    /*margin-top: 10px;*/
    margin: 0 !important;
  }
  .stepwizard-row {
    display: table-row;
  }
  .stepwizard {
    display: table;
    width: 100%;
    position: relative;
  }
  .stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
  }
  .stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
  }
  .stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
  }
  .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 10px;
  }
  .red-color{color:red;}
.col-center{margin:0 auto; float:none;}
  .middie-position{
    -webkit-flexbox;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
    justify-content: center;
  }
  .{margin:0 auto; float:none;}
</style>

</head>
<body>
  <?php
 
  $bTnCheckount       = 'btn-default';
  $bTnDatabase        = 'btn-default';
  $bTnSite            = 'btn-default';
  $bTnDone            = 'btn-default';
  $bTnPurchasekey     = 'btn-default';

  $dsCheckount       = 'disabled="disabled"';
  $dsDatabase        = 'disabled="disabled"';
  $dsSite            = 'disabled="disabled"';
  $dsDone            = 'disabled="disabled"';
  $dsPurchasekey     = 'disabled="disabled"';

  if($checkout == 1) {
    $bTnCheckount = 'btn-info';
    $dsCheckount ='';
  }

  if($purchasekey == 1) {
    $bTnPurchasekey = 'btn-info';
    $dsPurchasekey ='';
  }

  if($database == 1) {
    $bTnDatabase = 'btn-info';
    $dsDatabase = '';
  }

  if($site == 1) {
   $bTnSite    = 'btn-info';
   $dsSite = '';
 }

 if($done == 1) {
   $bTnDone    = 'btn-info';
   $dsDone = '';
 }


 ?>
 <div class="container"></br>
  <div class="row">
    <div class="col-sm-8 col-center">


      <div class="panel panel-info">
        <div class="panel-heading">

          <div class="stepwizard">
          <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
              <a href="<?php if($checkout == 1) { echo base_url('install/index'); } else { echo '#'; }?>" type="button" class="btn btn-circle <?=$bTnCheckount?>" <?=$dsCheckount?> >1</a>
              <p>Step 1</p>
            </div>
            <div class="stepwizard-step">
              <a href="<?php if($purchasekey == 1) { echo base_url('install/purchasekey'); } else { echo '#'; }?>" type="button" class="btn btn-circle <?=$bTnPurchasekey?>" <?=$dsPurchasekey?> >2</a>
              <p>Step 2</p>
            </div>
            <div class="stepwizard-step">
              <a href="<?php if($database == 1) { echo base_url('install/database'); } else { echo '#'; }?>" type="button" class="btn btn-circle <?=$bTnDatabase?>" <?=$dsDatabase?> >3</a>
              <p>Step 3</p>
            </div>
            <div class="stepwizard-step">
              <a href="<?php if($site == 1) { echo base_url('install/site'); } else { echo '#'; }?>" type="button" class="btn btn-circle <?=$bTnSite?>" <?=$dsSite?> >4</a>
              <p>Step 4</p>
            </div>
            <div class="stepwizard-step">
              <a href="<?php if($done == 1) { echo base_url('install/done'); } else { echo '#'; }?>" type="button" class="btn btn-circle <?=$bTnDone?>" <?=$dsDone?> >5</a>
              <p>Step 5</p>
            </div>
          </div>
        </div>
    </div>







        </div>
      </div>























  </div>

  <?php
  $this->load->view($subview);
  ?>
</div>
</body>
</html>
