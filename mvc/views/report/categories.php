<style type="text/css">
.img-height{
  height: 100px;
  width: 100px;
  float: left;
  margin-right: 10px;
}
#print_areas h3{line-height: 3px !important;}
.btn-primary{background-color: #19446a}
.btn-primary:hover{background-color: #1d4d77}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('reports_categories')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/report')?>"><?=$this->lang->line('reports_reports')?></a></li>
      <li><?=$this->lang->line('reports_categories')?></li>
    </ol>
  </section>


  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('reports_reports')?></h5>
        <div class="box-tools pull-right">
          <button class="btn btn-inline btn-custom btn-md prints"><i class="fa fa-print"></i> <?=$this->lang->line('report_print')?></button>
        </div>
      </div>
      <div class="box-body" id='print_areas'>
        <div>
          <div class="row">
            <div class=" col-xs-4">
              <img class="img-responsive img-height" src="<?=base_url('uploads/images/'.settings()->logo)?>">
              <div>
                <h4 class="text-capitalize"><?=settings()->company_name?></h4>
                <span><?=settings()->address?></span><br>
                <span>Phone: <?=settings()->phone?></span><br>
                <span>Email: <?=settings()->email?></span>
              </div>
            </div>
            <div class=" col-xs-8 text-center">
              <h3><?=$this->lang->line('reports_categories')?></h3>
              <span></span><br>
              <span><?=date('d-M-Y')?></span>
            </div>
          </div>
        </div><br>
        <div id="hide-table">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?=$this->lang->line('report_code')?></th>
                  <th><?=$this->lang->line('categories_name')?></th>
                  <th><?=$this->lang->line('categories_book')?></th>
                  <th><?=$this->lang->line('report_remark')?></th>
                </tr>
              </thead>
              <tbody >
                <?php if(count($categories)) { $i=0; foreach($categories as $category) { $i++; ?>
                <tr>
                  <td data-title="#"><?=$i?></td>
                  <td data-title="<?=$this->lang->line('report_code')?>"><?=ucfirst($category->categories_code)?></td>
                  <td data-title="<?=$this->lang->line('categories_name')?>"><?=ucfirst($category->categories_name)?></td>
                  <td data-title="<?=$this->lang->line('categories_book')?>">
                    <?php   
                      $total_sum=0; 
                      foreach ($booklist as $book) {
                        if (($book->book_category) == ($category->categoriesID)){
                          $total_sum+= $book->book_quantity;
                        }
                      }  
                      echo "<b>".$total_sum."</b>";
                    ?>
                  </td>
                  <td data-title="<?=$this->lang->line('categories_remark')?>">
                   
                  </td>
                </tr>
                <?php } } ?>
              </tbody>

                  
            </table>
        </div>

      </div>
    </div>
  </section>
</div>