<style type="text/css">
.bg-default{border: 1px solid#ccc}
.bg-default:hover{ color:#000 }
.bg-default .small-box-footer{color:#000}
.bg-default .small-box-footer:hover{color:#000}
.bg-primary{background-color: #19446a}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('reports_reports')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><?=$this->lang->line('reports_reports')?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <?php if(permissionChecker('report_member')){?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h4><?=$this->lang->line('reports_member')?></h4>
              <h4>&nbsp;</h4>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="<?=base_url('report/member')?>" class="small-box-footer"><?=$this->lang->line('reports_details')?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }?>
      <?php if(permissionChecker('report_book')){?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h4><?=$this->lang->line('reports_book')?></h4>
              <h4>&nbsp;</h4>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="<?=base_url('report/book')?>" class="small-box-footer"><?=$this->lang->line('reports_details')?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }?>
      <?php if(permissionChecker('report_payment')){?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h4><?=$this->lang->line('reports_payment')?></h4>
              <h4>&nbsp;</h4>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="<?=base_url('report/payment')?>" class="small-box-footer"><?=$this->lang->line('reports_details')?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }?>
      <?php if(permissionChecker('report_categories')){?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h4><?=$this->lang->line('reports_categories')?></h4>
              <h4>&nbsp;</h4>
            </div>
            <div class="icon">
              <i class="fa fa-cubes"></i>
            </div>
            <a href="<?=base_url('report/categories')?>" class="small-box-footer"><?=$this->lang->line('reports_details')?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }?>
<!--     </div>

    <div class="row"> -->
     <?php if(permissionChecker('reports_stock')){?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="small-box bg-default">
            <div class="inner">
              <h4><?=$this->lang->line('reports_stock')?></h4>
              <h4>&nbsp;</h4>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?=base_url('reports/stock')?>" class="small-box-footer"><?=$this->lang->line('reports_details')?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }?>
      <?php if(permissionChecker('reports_itemsales')){?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="small-box bg-default">
            <div class="inner">
              <h4><?=$this->lang->line('reports_item_sales')?></h4>
              <h4>&nbsp;</h4>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?=base_url('reports/itemsales')?>" class="small-box-footer"><?=$this->lang->line('reports_details')?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }?>
      <?php if(permissionChecker('reports_expenses')){?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="small-box bg-default">
            <div class="inner">
              <h4><?=$this->lang->line('reports_expenses')?></h4>
              <h4>&nbsp;</h4>
            </div>
            <div class="icon">
              <i class="fa fa-credit-card"></i>
            </div>
            <a href="<?=base_url('reports/expenses')?>" class="small-box-footer"><?=$this->lang->line('reports_details')?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }?>
      <?php if(permissionChecker('reports_wastage')){?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="small-box bg-default">
            <div class="inner">
              <h4><?=$this->lang->line('reports_wastage')?></h4>
              <h4>&nbsp;</h4>
            </div>
            <div class="icon">
              <i class="fa fa-recycle"></i>
            </div>
            <a href="<?=base_url('reports/wastage')?>" class="small-box-footer"><?=$this->lang->line('reports_details')?> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      <?php }?>
    </div>
  </section>
</div>

