
<br>
<div class="row setup-content" >
  	<div class="col-sm-8 col-center">
    <!-- <div class="row"> -->
	    <div class="panel panel-info">
	        <div class="panel-heading"><h4>Database Config</h4></div>
	        <div class="panel-body ">
	        	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
	        		<input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
				<?php
					if(form_error('host'))
						echo "<div class='form-group has-error' >";
					else
						echo "<div class='form-group' >";
					?>
					<label for="host" class="col-sm-2 col-md-offset-2 col-sm-offset-2 control-label">
						<p>Hostname</p>
					</label>
					<div class="col-sm-6">
						<input type="text" class="custom-input-field" id="host" name="host" value="<?=set_value('host')?>" >
						<span class=" control-label"><?php echo form_error('host'); ?></span>
					</div>	
				</div>


				<?php
					if(form_error('database'))
						echo "<div class='form-group has-error'>";
					else
						echo "<div class='form-group'>";
					?>
					<label for="database" class="col-sm-2 col-md-offset-2 col-sm-offset-2 control-label">
						<p>Database</p>
					</label>
					<div class="col-sm-6">
						<input type="text" class="custom-input-field" id="database" name="database" value="<?=set_value('database')?>" >
						<span class=" control-label"><?php echo form_error('database'); ?></span>
					</div>			
				</div>

				<?php
					if(form_error('user'))
						echo "<div class='form-group has-error' >";
					else
						echo "<div class='form-group' >";
					?>
					<label for="user" class="col-sm-2 col-md-offset-2 col-sm-offset-2 control-label">
						<p>Username</p>
					</label>
					<div class="col-sm-6">
						<input type="text" class="custom-input-field" id="user" name="user" value="<?=set_value('user')?>" >
						<span class="control-label"><?php echo form_error('user'); ?></span>
					</div>
				</div>

				<?php
					if(form_error('password'))
						echo "<div class='form-group has-error' >";
					else
						echo "<div class='form-group' >";
					?>
					<label for="password" class="col-sm-2 col-md-offset-2 col-sm-offset-2 control-label">
						<p>Password</p>
					</label>
					<div class="col-sm-6">
						<input type="password" class="custom-input-field" id="password" name="password" value="<?=set_value('password')?>" >
					</div>
					<span class="col-sm-4 control-label">
						<?php echo form_error('password'); ?>
					</span>
				</div>
	        </div>

	        <div class="panel-footer">
	        	<div class="row">
	        		<div class="col-sm-12 col-xs-12">
			        	<div class="pull-left">
			        		<a href="<?=base_url('install/index')?>" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> PREVIOUS</a>
			        	</div>
						<!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
						<div class="pull-right">
							<button type="submit" class="btn btn-success">NEXT <i class="fa fa-long-arrow-right"></i></button>
						</div>
					</div>
				</div>
				</form>
			</div>
	  	</div>
	</div>
</div>