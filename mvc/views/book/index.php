<style type="text/css">
table.dataTable tbody td {vertical-align:middle;}
.col-sm-4{width:32.3333333333%; margin: 1px;}
.custom-barcode-css{border: 1px dotted;padding: 2px;}
.custom-barcode-css p{margin: 0;line-height: 1;font-size: smaller;text-align: center;}
.custom-barcode-css h6{text-align: center;margin: 0;font-weight: bold;}
.custom-barcode-css img{height: 40px;width: 140px;}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?=$this->lang->line('book_book')?></h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
      <li><a href="<?=base_url('/book')?>"><?=$this->lang->line('book_book')?></a></li>
    </ol>
  </section>



  <section class="content">
    <div class="box box-solid">
      <div class="box-header header-custom">
        <h5 class="box-title"><i class="fa fa-list"></i> <?=$this->lang->line('book_list')?></h5>
        <div class="box-tools pull-right">
          <?php if(permissionChecker('book_add')){?>
            <a href="<?=base_url('book/add')?>" class="btn btn-inline btn-custom btn-md"><i class="fa fa-plus"></i> <?=$this->lang->line('book_add_book')?></a>
          <?php }?>
        </div>
      </div>
      <div class="box-body">
        <div id="hide-table">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th><?=$this->lang->line('book_photo')?></th>
                <th><?=$this->lang->line('book_code')?></th>
                <th><?=$this->lang->line('book_name')?></th>
                <th><?=$this->lang->line('book_writer')?></th>
                <th><?=$this->lang->line('book_edition')?></th>
                <th><?=$this->lang->line('book_quantity')?></th>
                <th><?=$this->lang->line('book_availability')?></th>
                <th><?=$this->lang->line('book_rack_no')?></th>
                <th><?=$this->lang->line('book_status')?></th>
                <th><?=$this->lang->line('book_action')?></th>
              </tr>
            </thead>
            <tbody>
              <?php if(count($book)) { $i=0; foreach($book as $val) { $i++; ?>
              <tr>
                <td data-title="#"><?=$i?></td>
                <td data-title="<?=$this->lang->line('book_photo')?>"><img src="<?=book_img($val->book_photo)?>" class="profile_img" alt="<?=$val->book_name?>"></td>
                <td data-title="<?=$this->lang->line('book_code')?>"><?=$val->book_code?></td>
                <td data-title="<?=$this->lang->line('book_name')?>" class="text-capitalize"><?=$val->book_name?></td>
                <td data-title="<?=$this->lang->line('book_writer')?>"><?=ucwords(isset($writer[$val->book_writerID]) ? $writer[$val->book_writerID] : '&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('book_edition')?>" class="text-capitalize"><?=$val->book_edition?></td>
                <td data-title="<?=$this->lang->line('book_quantity')?>"><?=$val->book_quantity?></td>
                <td data-title="<?=$this->lang->line('book_availability')?>"><?=(!empty($val->book_availability)?$val->book_availability:'&nbsp;')?></td>
                <td data-title="<?=$this->lang->line('book_rack_no')?>"><?=$val->book_rack_no?></td>
                <td data-title="<?=$this->lang->line('book_status')?>">
                  <div class="onoffswitch-small" id="<?=$val->bookID?>">
                    <input type="checkbox" id="myonoffswitch<?=$val->bookID?>" class="onoffswitch-small-checkbox" name="categorys_status" <?php if($val->book_status === '1') echo "checked='checked'"; ?><?=permissionChecker('book_status')?'':'disabled'?>>
                    <label for="myonoffswitch<?=$val->bookID?>" class="onoffswitch-small-label">
                      <span class="onoffswitch-small-inner"></span>
                      <span class="onoffswitch-small-switch <?=permissionChecker('book_status')?'':'hidden'?>"></span>
                    </label>
                  </div>
                </td>
                <td data-title="<?=$this->lang->line('book_action')?>">
                  <?=view_btn('book_view', $val->bookID);?>
                  <?=barcode('book_barcode', $val->bookID);?>
                  <?=(permissionChecker('book_edit')?btn_edit_show('book/edit/'.$val->bookID,$this->lang->line('edit')):'');?>
                  <?=delete_btn('book_delete', $val->bookID); ?>
                </td>
              </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- add modal-->
<div class="modal fade" id="details" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="icofont-book"></i></span>&nbsp;<?=$this->lang->line('book_details')?></h4>
      </div>
      <div class="modal-body">
        <div class="row" id="showData">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- View barcode   -->
<div class="modal fade" id="barcode" tabindex="-1">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-barcode"></i>&nbsp; Barcode</h4>
      </div>

      <div class="modal-body">
        <table  class="table dt-responsive zero-border" id='print_areas'>
          <tbody id="showBarcode">
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-sm btn-info prints" id="get_print"><span class="glyphicon glyphicon-print" ></span>&nbsp;Print</button>
        <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>