<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=settings()->company_name?> <?=(!empty($title)?'| '.$title : '')?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/Ionicons/css/ionicons.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/square/blue.css')?>">
  <style type="text/css">html body{ height: none !important;}.glyphicon-xl{font-size: 100px;color: skyblue;} .h1-header{font-size: 36px;} .login-box-msg{padding:0px !important;}.field-icon {  float: right; margin-left: -25px; margin-top: -25px; position: relative; z-index: 2;margin-right: 7px;}.form-control-feedback{line-height:17px !important;}</style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?=base_url('/')?>" class="text-capitalize"><b><?=settings()->company_name;?></b></a>
  </div>
  <!-- /.login-logo -->
    <?php if ($userID == NULL) { ?>
  <div class="login-box-body">
    <p class="login-box-msg"><span class="glyphicon glyphicon-lock glyphicon-xl"></span></p>
    <p class="login-box-msg h1-header"><?=$this->lang->line('validation')?></p>
    <p class="login-box-msg"><?=$this->lang->line('validation_sms')?></p>
    <br>
    <!-- VALIDATION PART -->
    <form action="<?=base_url('login/validation')?>" method="post">
      <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
      
      <div class="form-group <?=form_error('validation_code') ? 'has-error' : ''?>">
        <label for="validation_code"><?=$this->lang->line('validation_code')?></label> <span class="text-red">*</span>
        <input type="text" class="form-control" id="validation_code" name="validation_code" value="<?=set_value('validation_code')?>" placeholder="<?=$this->lang->line('validation_code')?>"> 
        <?=form_error('validation_code','<div class="text-red">', '</div>')?>
      </div>
    <br>
      <div class="row">
        <div class="col-xs-8">
          <a href="<?= base_url('login')?>"><?=$this->lang->line('retry')?></a>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><?=$this->lang->line('verify_button')?></button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>


  <?php }else { ?>

  

      <div class="login-box-body">
      <br>

      <!-- RESET PASSWORD PART -->
      <p class="login-box-msg"><span class="glyphicon glyphicon-keys glyphicon-xl"></span></p>
      <p class="login-box-msg h1-header"><?=$this->lang->line('reset_password')?></p>
      <p class="login-box-msg"><?=$this->lang->line('reset_password_sms')?></p>
      <br>
      <form action="<?=base_url('login/reset')?>" method="post">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
        <input type="hidden" name="userID" value="<?=$userID->userID?>" readonly/>
        
        <div class="form-group <?=form_error('new_password') ? 'has-error' : ''?>">
          <label for="new_password"><?=$this->lang->line('new_password')?></label> <span class="text-red">*</span>
          <input type="password" class="form-control" id="new_password" name="new_password" value="<?=set_value('new_password')?>" placeholder="<?=$this->lang->line('new_password')?>"> 
          <span toggle="#new_password" class="field-icon  glyphicon glyphicon-eye-open toggle-password"></span> 
          <?=form_error('new_password','<div class="text-red">', '</div>')?>
        </div>
        
        <div class="form-group <?=form_error('confirm_new_password') ? 'has-error' : ''?>">
          <label for="confirm_new_password"><?=$this->lang->line('confirm_new_password')?></label> <span class="text-red">*</span>
          <input type="password" class="form-control" id="confirm_new_password" name="confirm_new_password" value="<?=set_value('confirm_new_password')?>" placeholder="<?=$this->lang->line('confirm_new_password')?>">
          <span toggle="#confirm_new_password" class="field-icon  glyphicon glyphicon-eye-open toggle-password"></span>
          <?=form_error('confirm_new_password','<div class="text-red">', '</div>')?>
        </div>

      <br>
        <div class="row">
          <div class="col-xs-8">
            <a href="<?= base_url('login')?>"><?=$this->lang->line('retry')?></a>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>



<?php } ; ?>

















  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?=base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
<script type="text/javascript">
  $(".toggle-password").click(function() {
    $(this).toggleClass("glyphicon-eye-open glyphicon-eye-close");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
</script>
</body>
</html>
