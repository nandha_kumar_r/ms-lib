<style>
    .profile-view-tab {
        font-size: 16px;
    }
    .profile-view-tab span {
        font-weight: 600;
    }

    .profile-view-tab p span {
        display: inline-block;
        width: 35%;
    }

</style>
<div class="content-wrapper">
    <section class="content-header">
  		<h1>User</h1>
  		<ol class="breadcrumb">
            <li><a href="<?=base_url('/')?>"><i class="fa fa-dashboard"></i> Home</a></li>
  			<li><a href="<?=base_url('/user')?>"><i class="fa fa-dashboard"></i> User</a></li>
  			<li class="active">Add</li>
  		</ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img src="<?=profile_img($user->photo)?>" class="profile_user_img img-responsive img-circle " alt="">
                        <h3 class="profile-username text-center"><?=$user->name?></h3>
                        <p class="text-muted text-center"><?=isset($usertypes[$user->usertypeID]) ? $usertypes[$user->usertypeID] : ''?></p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Gender</b> <a class="pull-right"><?=ucfirst($user->gender)?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Date Of Birth</b> <a class="pull-right"><?=app_date($user->dob)?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Phone</b> <a class="pull-right"><?=ucfirst($user->phone)?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="profile">
                            <div class="panel-body profile-view-dis">
                                <div class="row">
                                    <div class="profile-view-tab col-md-6">
                                        <p><span>Joining Date </span>: <?=app_date($user->jod)?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span>Religion </span>: <?=$user->religion?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span>Email </span>: <?=$user->email?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span>Address </span>: <?=$user->address?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span>Username </span>: <?=$user->username?></p>
                                    </div>
                                    <div class="profile-view-tab col-md-6">
                                        <p><span>Status</span>: <?=($user->status ==1) ? "<span class='text-green'>Active</span>" : "<span class='text-red'>Block</span>"?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>