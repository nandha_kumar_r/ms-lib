<div class="content-wrapper">
    <section class="content-header">
        <h1><?=$this->lang->line('member_member')?></h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url('/')?>"><i class="fa fa-laptop"></i></a></li>
            <li><a href="<?=base_url('/member')?>"> <?=$this->lang->line('member_member')?></a></li>
            <li class="active"><?=$this->lang->line('member_add')?></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-custom-border">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                        <input type="hidden" class="form-control" name="member_code" id="member_code" value="<?=settings()->member_prefixe;membercode();?>" readonly/>
                        <div class="box-body">
                         <fieldset class="border-fieldset">
                            <legend class="border-legend"><?=$this->lang->line('member_info')?></legend>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('name') ? 'has-error' : ''?>">
                                        <label for="name"><?=$this->lang->line('member_name')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name')?>" placeholder="<?=$this->lang->line('member_name')?>">
                                        <?=form_error('name','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('father_name') ? 'has-error' : ''?>">
                                        <label for="father_name"><?=$this->lang->line('member_father_name')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="father_name" name="father_name" value="<?=set_value('father_name')?>" placeholder="<?=$this->lang->line('member_father_name')?>">
                                        <?=form_error('father_name','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('mother_name') ? 'has-error' : ''?>">
                                        <label for="mother_name"><?=$this->lang->line('member_mother_name')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="mother_name" name="mother_name" value="<?=set_value('mother_name')?>" placeholder="<?=$this->lang->line('member_mother_name')?>">
                                        <?=form_error('mother_name','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('member_date_of_birth') ? 'has-error' : ''?>">
                                        <label for="member_date_of_birth"><?=$this->lang->line('member_date_of_birth')?>
                                        </label> <span class="text-red">*</span>
                                        <input type="text" class="form-control datepicker_year" id="member_date_of_birth" name="member_date_of_birth" value="<?=set_value('member_date_of_birth')?>" placeholder="<?=$this->lang->line('member_date_of_birth')?>">
                                        <?=form_error('member_date_of_birth','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="member_nid_no"><?=$this->lang->line('member_nid_no')?></label>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="<?=$this->lang->line('member_nid_no_tooltip')?>"></i>
                                        <input type="text" class="form-control" id="member_nid_no" name="member_nid_no" value="<?=set_value('member_nid_no')?>" placeholder="<?=$this->lang->line('member_nid_no')?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('member_occupation') ? 'has-error' : ''?>">
                                        <label for="member_occupation"><?=$this->lang->line('member_occupation')?>
                                        </label> <span class="text-red">*</span>
                                        <input type="text" class="form-control" id="member_occupation" name="member_occupation" value="<?=set_value('member_occupation')?>" placeholder="<?=$this->lang->line('member_occupation')?>">
                                        <?=form_error('member_occupation','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                        <div class="form-group <?=form_error('membership') ? 'has-error' : ''?>">
                                            <label for="membership"><?=$this->lang->line('member_membership')?></label>&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="<?=$this->lang->line('member_membership_tooltip')?>"></i><span class="text-red">*</span>
                                            <?php 
                                            $membershipArray[0] = $this->lang->line('member_please_select');
                                            if(count($membership)){
                                              foreach ($membership as $val){
                                                $membershipArray[$val->membershipID] = $val->membership_name;
                                            }
                                        }
                                        echo form_dropdown('membership', $membershipArray,set_value('membership'),'id="membership" class="form-control select2"');
                                        ?>
                                        <?=form_error('membership','<div class="text-red">', '</div>')?> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('member_gender') ? 'has-error' : ''?>">
                                        <label for="member_gender"><?=$this->lang->line('member_gender')?></label><span class="text-red">*</span>
                                        <?php 
                                        $genderArray['0'] = $this->lang->line('member_please_select');
                                        $genderArray['male'] = $this->lang->line('member_male');
                                        $genderArray['female'] = $this->lang->line('member_female');
                                        $genderArray['third_gender'] = $this->lang->line('member_third_gender');
                                        echo form_dropdown('member_gender', $genderArray,set_value('member_gender'),'id="member_gender" class="form-control"');
                                        ?>
                                        <?=form_error('member_gender','<div class="text-red">', '</div>')?> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="member_blood_group"><?=$this->lang->line('member_blood_group')?></label>
                                        <?php 
                                            $bloodgroupArray['0']   = $this->lang->line('member_please_select');
                                            $bloodgroupArray['ab+'] = 'AB+';
                                            $bloodgroupArray['ab-'] = 'AB-';
                                            $bloodgroupArray['a+']  = 'A+';
                                            $bloodgroupArray['a-']  = 'A-';
                                            $bloodgroupArray['b+']  = 'B+';
                                            $bloodgroupArray['b-']  = 'B-';
                                            $bloodgroupArray['o+']  = 'O+';
                                            $bloodgroupArray['o-']  = 'O-';
                                            echo form_dropdown('member_blood_group', $bloodgroupArray,set_value('member_blood_group'),'id="member_blood_group" class="form-control"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('member_religion') ? 'has-error' : ''?>">
                                        <label for="member_religion"><?=$this->lang->line('member_religion')?></label><span class="text-red">*</span>
                                        <?php 
                                            $religionArray['0']   = $this->lang->line('member_please_select');
                                            $religionArray['buddhism'] = 'Buddhism';
                                            $religionArray['christianity'] = 'Christianity';
                                            $religionArray['hinduism']  = 'Hinduism';
                                            $religionArray['islam']  = 'Islam';
                                            echo form_dropdown('member_religion', $religionArray,set_value('member_religion'),'id="member_religion" class="form-control"');
                                        ?>
                                        <?=form_error('member_religion','<div class="text-red">', '</div>')?> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group <?=form_error('member_phone') ? 'has-error' : ''?>">
                                        <label for="member_phone"><?=$this->lang->line('member_phone')?></label> <span class="text-red">*</span>
                                        <input type="text" class="form-control is_numeric" id="member_phone" name="member_phone" value="<?=set_value('member_phone')?>" placeholder="<?=$this->lang->line('member_phone')?>">
                                        <?=form_error('member_phone','<div class="text-red">', '</div>')?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="member_email"><?=$this->lang->line('member_email')?></label>
                                        <input type="text" class="form-control" id="member_email" name="member_email" value="<?=set_value('member_email')?>" placeholder="<?=$this->lang->line('member_email')?>">
                                    </div>
                                </div>
                             </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="member_since"><?=$this->lang->line('member_since')?></label>
                                    <input type="text" class="form-control datepicker" id="member_since" name="member_since" value="<?=set_value('member_since',date('d-m-Y'))?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?=form_error('member_address') ? 'has-error' : ''?>">
                                    <label for="member_address"><?=$this->lang->line('member_address')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control" id="member_address" name="member_address" value="<?=set_value('member_address')?>" placeholder="<?=$this->lang->line('member_address')?>">
                                    <?=form_error('member_address','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?=form_error('photo') ? 'has-error' : ''?>">
                                    <label for="photo"><?=$this->lang->line('member_photo')?></label> 
                                    <input type="file" class="form-control" id="photo" name="photo"/>
                                    <?=form_error('photo','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    
                    <fieldset class="border-fieldset">
                        <legend class="border-legend"><?=$this->lang->line('member_login_info')?></legend>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group <?=form_error('member_username') ? 'has-error' : ''?>">
                                    <label for="member_username"><?=$this->lang->line('member_username')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control" id="member_username" name="member_username" value="<?=set_value('member_username')?>" placeholder="<?=$this->lang->line('member_username')?>">
                                    <?=form_error('member_username','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?=form_error('member_password') ? 'has-error' : ''?>">
                                    <label for="member_password"><?=$this->lang->line('member_password')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control" id="member_password" name="member_password" value="<?=set_value('member_password')?>" placeholder="<?=$this->lang->line('member_password')?>">
                                    <?=form_error('member_password','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group <?=form_error('member_confirm_password') ? 'has-error' : ''?>">
                                    <label for="member_confirm_password"><?=$this->lang->line('member_confirm_password')?></label> <span class="text-red">*</span>
                                    <input type="text" class="form-control" id="member_confirm_password" name="member_confirm_password" value="<?=set_value('member_confirm_password')?>" placeholder="<?=$this->lang->line('member_confirm_password')?>">
                                    <?=form_error('member_confirm_password','<div class="text-red">', '</div>')?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-past"><?=$this->lang->line('member_add_member')?></button>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
</div>

<script>
$('.datepicker').datepicker({
    autoclose: true,
    format : 'dd-mm-yyyy',
})
$('.datepicker_year').datepicker({
    autoclose: true,
    format : 'dd-mm-yyyy',
    viewMode: "months", 
    minViewMode: "days",
    startView: 2
})
</script>